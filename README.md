# Bachelor Thesis
This repository houses the source code and the LaTeX files for the thesis: Aerosol dynamics in human lungs.
The pdf is located at https://respiratory.tudelft.nl

You can use the code to calculate deposition fractions in the respiratory tract, based on a model from doi:10.1016/j.jaerosci.2004.08.008

If you end up extending this model for your own thesis, please notify me.
I'm curious as to your results.

## LaTeX compilation
Compile using pdflatex and biber.

## Python
Take a look at results.py to generate results, and running model.py will run a simulation with default arguments, and plot the results.