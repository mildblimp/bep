\chapter{Conclusions and recommendations}
\label{chap:conclusion}

In this study, we developed a 1-dimensional model describing the behaviour of aerosols in the human respiratory tract.
It proved capable of accurately calculating the local particle deposition, while taking into account both particle properties and respiratory conditions of the patient.
The model was based on a model developed by~\textcite{eulerian}, who investigated particle deposition fractions for a range of different particle sizes.
We validated our results by comparing them with literature, and we obtained satisfactory agreement across the entire spectrum of particle sizes.

Our study had a similar focus as the aforementioned one, and we extended their work by considering the deposition fraction under a range of different respiratory conditions.
This was facilitated by the little computational effort required to generate results, requiring just seconds to calculate the deposition fraction during a breathing cycle.
In contrast, present 3-dimensional models require millions of control volumes to model only a part of the RT, which demonstrates the usefulness of our simplistic 1-dimensional approach.

After verifying that the numerical scheme was convergent, we did extensive verification on both the concentration profile as well as the (local) deposition fraction.
This was used to perform a sensitivity analysis of the computational parameters on the model, which proved to respond in a consistent way.
Varying the physiological parameters of the patient was done next, where we varied the tidal volume and the breathing rate and assessed their influence on the deposition fraction.
Both an increase in tidal volume as well as an increase in the breathing rate proved to decrease the deposition fraction across the spectrum of particle sizes, apart from at the tails of the distribution.
Despite this, an increase in tidal volume resulted in increased normalised absorption, due to the extra air intake.

Apart from varying physiological parameters, we also looked at particle-specific parameters like the density.
It was reported that an increase in particle density increased the deposition fraction for the larger class of particles (\(>\SI{0.1}{\micro\metre}\)).

There are a number of improvements to this study that can be realized in a future work.
We briefly experimented with different morphological models such as that of Yeh and Schum, but we did not implement it in its entirety.
By using this morphological description, it will be possible to report lobar deposition instead of merely the average deposition for the whole lungs.
In addition, we did not model hydroscopic growth, which has a big effect on particle deposition due to the high relative humidity (\(\approx99.5\%\)) prevailing in the human lungs~\cite{eulerian}.
If this growth is also implemented, the model could be used to accurately model regional deposition while taking into account all of the important physical interactions that aerosols undergo in the lungs.

Of lesser importance, a higher order numerical scheme can be developed in an attempt to further reduce the computational time required to run simulations.
Moreover, we made various assumptions that can be challenged, such as that the terminal settling velocity is reached instantly, and that impaction is the same during inspiration and expiration.
Addressing these issues can improve the model's predictions, but care must be taken as not to lose sight of the bigger picture.
A numerical model can be used as a benchmark, but empirical verification should always be employed in a real world scenario.
