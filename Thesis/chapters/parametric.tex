\chapter{Parametric studies}
\label{chap:results}
Knowing how the breathing rate affects aerosol deposition is important, as the recent example of bush fires in Australia (2019-20) demonstrates.
This bushfire season, also known as the Black Summer, scorched over 18 million hectares of land, and killed more than 400 people.
Smoke inhalation was the cause of 417 of these deaths~\cite{borchers2020unprecedented}.
Bushfire smoke covered large areas of land, and sports events were cancelled because of concerns of smoke inhalation, while normal exercise was discouraged.
In this parametric study, we will examine how the breathing pattern affects deposition rates and total deposition of aerosols.
Exercise results in an elevated heart rate and quickened breathing, so the expectation is that the total deposition will increase - we wish to quantify this.

In addition, the world is currently plagued by the COVID-19 pandemic, and preliminary case studies have indicated that airborne transmission plays a profound role in spreading the virus~\cite{COVIDaerosol}.
For this reason, we will pay special attention to particles of around \SI{1}{\micro\metre} in diameter, which is the predominant size of coughed droplets~\cite{coughsize}.

\section{Sensitivity runs}
Before investigating the effect of different breathing conditions, we will do a series of sensitivity runs to check how the model responds to other parametric variations.
These variations differ from changing the breathing conditions in that they pertain to the computational model in question, and not the environmental conditions of the patient.

All of the simulations have been run with a time step of \(\Delta t=\SI{0.1}{\second}\), and a grid size of 480 nodes.
These nodes have been distributed equally over the different generations, with 20 nodes in each generation.
With these parameters, a single simulation takes a few seconds to complete on a personal computer.
Examining the effect of particle size (to produce plots like Figure~\ref{fig:weibel_vs_yehschum}) generally takes a few minutes for a particular set of initial parameters, making it very easy to quickly analyse a number of parametric variations.

\subsection{Geometry rescaling}
The first of these is checking how the scaling the generation lengths and radii affects the deposition fraction.
It is customary to scale Weibel's lung model by a factor of \((V_{\text{FRC}}/\SI{4.8}{\litre})^{1/3}\), as is explained in Appendix~\ref{app:data}.
The deposition fractions with and without this scaling factor are shown in Figure~\ref{subfig:rescale}.

For lungs with a \(V_{\text{FRC}}=\SI{3300}{\milli\litre}\), the rescaling of the geometry makes almost no difference.
This effect becomes more pronounced when the functional residual capacity becomes smaller, which makes the scaling factor larger.
A possible explanation for the slightly higher deposition fractions when the geometry is rescaled is that the bronchi and bronchioles become smaller, resulting in more deposition due to diffusion and impaction.
This is also seen in Figure~\ref{subfig:rescale_local}, which explores the effect of the rescaling on the local deposition fraction.
Here, the deposition increases in the first few generations, but seems to equalise beyond generation 16, where the effect of the rescaling is offset by the varying diameter \(d_T\) due to breathing mechanics.
\begin{figure}[h]
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/rescale_geometry.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{}
    \label{subfig:rescale}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/rescale_geometry_local.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{}
    \label{subfig:rescale_local}
  \end{subfigure}
  \caption{%
  (\subref{subfig:rescale}) Deposition fractions with (dotted) and without (dot-dashed) a rescaling of Weibel's model, for a range of different particle sizes.
  The dashed line also includes a rescale but with~\(V_{\text{FRC}}=\SI{2400}{\milli\litre}\), and (\subref{subfig:rescale_local}) the local deposition fraction for the same parameters but with \(d=\SI{4}{\micro\metre}\).}
\end{figure}

\subsection{Weibel's lung geometry or Yeh and Schum's}
Weibel's model ``A'' is only one of many lung models devised over the years.
Some vary in the volume of the alveolar sacs or the number of generations, others include the asymmetry of the left lung.
We will look at a proposed geometry by Yeh and Schum, which varies in the generation length and radius, making a comparison simple.
The different lung geometries are compared in Figure~\ref{fig:weibel_vs_yehschum}.

Note that in these calculations we have discounted the final generation in Yeh and Schum's model, which is why the difference between the two models is very pronounced.
Across the entire spectrum, Weibel's model reports more deposition than Yeh and Schum's.
The final generation of Yeh and Schum's model consists of \num{3e8} alveolar sacs each with a length of \SI{250}{\micro\metre} and a diameter of \SI{300}{\micro\metre}~\cite{weibeldata}.
Future work could look into implementing Yeh and Schum's typical path lung model in its entirety, which would include quasi 3-dimensional geometries and allow for more accurate comparison.
The quasi 3d structure is due to their model's asymmetry - they specify six different segments, considering the difference between the right and left lung, and their top, middle and lower lobes.
Each of these six segments has their own number of alveolar sacs.
Implementing these corrections, deposition fractions would be more akin to those seen in Weibel's model, as can be seen in~\cite{compilation}.
\begin{figure}
  \centering
  \input{"../Images/Graphs/weibel_vs_yehschum.tex"}
  \caption{%
  Deposition fractions for the parameters of Weibel's model ``A'' (dotted) and Yeh and Schum's (dashed).
  For the latter model, the final generation has not been taken into consideration.
  }
  \label{fig:weibel_vs_yehschum}
\end{figure}

\subsection{Time-dependent vs fixed geometry}
Lastly, we will look at the effect that the time-dependent geometry has on particle deposition.
For generations >16, the generation radii are allowed to increase or decrease to account for the varying lung capacity, as explained in Section~\ref{subsec:velocity}.
The effect of the time-varying geometry is shown in Figure~\ref{subfig:time_dependent_geometry}
\begin{figure}[h]
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/time_dependent_geometry.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{}
    \label{subfig:time_dependent_geometry}
\end{subfigure}
  ~
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/fixed_geometry_local.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{}
    \label{subfig:time_dependent_geometry_local}
  \end{subfigure}
  \caption{%
  (\subref{subfig:time_dependent_geometry}) Deposition fractions for time-dependent (dotted) and fixed lung geometry (dashed), and (\subref{subfig:time_dependent_geometry_local}) the local deposition fraction for the same parameters but with \(d=\SI{0.3}{\micro\metre}\).}
\end{figure}

The time-dependent geometry makes almost no difference on the deposition fraction across the range of particle sizes.
The slightly higher deposition fractions when the geometry is time-dependent can be explained by the fact that the wetted perimeter increases in the last few generations, increasing the deposition surface.
But the difference is very small, and is only seen for \(d=\SI{0.3}{\micro\metre}\), where the difference in deposition is largest.

\section{Deposition fractions under eupnoea and hyperpnea}
In this section we will investigate how the deposition fraction varies of a range of particle sizes under different respiratory conditions.
The main parameter that we will change is the tidal volume \(V_T\).
During exercise, the functional residual capacity decreases~\cite{SHARRATT1987313} and the tidal volume increases.
Figure~\ref{subfig:tidal_volume} shows the deposition fraction for a number of respiratory conditions.

For the most part, the deposition fraction decreases as the tidal volume increases.
At the tails, the process seems to reverse.
The results match closely with the results of~\textcite{eulerian}.
Unfortunately, we cannot compare the tail behaviour because the literature only tested the model under regular breathing circumstances.

Instead of looking at the deposition fraction, we can also look at how the total deposition develops over time, for different tidal volumes.
This will enable us to explain whether the banning of exercise in Australia during the bushfires was academically founded.
The cumulative absorption of aerosols is shown in Figure~\ref{subfig:concentration_vs_time}.
Indeed, we see that despite the smaller deposition fraction, the cumulative absorption of the larger tidal volume increases at almost twice the speed compared to the smaller one.
This of course, is entirely as expected, because the air intake rate is a lot higher.

Note that the ``normalised'' absorption in Figure~\ref{subfig:concentration_vs_time} is the total absorption assuming a normalised concentration at the inlet.
In this sense, the absorption is not ``normalised'' in the standard sense of the word.
We also notice the step-wise increase of absorption - absorption mostly occurs during the inspiration phase.

In addition to tidal volume variation, we also investigated the effect of breathing rate on the deposition fraction.
Breathing rates of 10, 12 and 15 breaths per minute result in an almost uniform decrease of the deposition fraction across the spectrum.
However, because of the similarity between this figure and Figure~\ref{subfig:tidal_volume}, we have not included it here.

\begin{figure}[h]
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/tidal_volume.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{}
    \label{subfig:tidal_volume}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/concentration_vs_time.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{}
    \label{subfig:concentration_vs_time}
  \end{subfigure}
  \caption{(\subref{subfig:tidal_volume}) Deposition fractions for tidal volumes of \SIlist{500;1000;2000;3000}{\milli\litre} (dotted to solid, respectively) for a range of particle sizes, and (\subref{subfig:concentration_vs_time}) the cumulative absorption during the first minute for particles of size \SI{0.3}{\micro\metre} with tidal volumes of \SIlist{1000;3000}{\milli\litre} (dotted and dashed, respectively).}
\end{figure}

\section{The effect of aerosol density}
In Section~\ref{subsec:depfrac} we briefly discussed the effect of particle density on the deposition fraction of different classes of particles.
This was in relation to the literature comparison, because it was unclear what parameters they ran their programme with, making the comparison more difficult.
Figure~\ref{fig:particle_density} show the effect of particle density on the deposition fraction.

Particle density does not seem to have a great effect for the smaller particles, whose deposition mechanism is mostly due to diffusion.
The larger classes of particles, whose main deposition mechanism is impaction, experience a greater effect.
The deposition fraction increases for particles bigger than about \SI{0.1}{\micro\metre}.
Overall, the trough for medium sized particles decreases and shifts to the left.

Cough droplets, which have a density of~\SI{1.0}{\gram\per\centi\metre\cubed} and a typical particle diameter of~\SI{1}{\micro\metre}~\cite{coughsize}, seem to deposit around 50\% of the time.
This indicates that COVID-19 borne droplets have a high chance of being absorbed in the lungs, but modelling the actual transmission risk is out of the scope of this thesis.
\begin{figure}
  \centering
  \input{"../Images/Graphs/particle_density.tex"}
  \caption{Deposition fractions for particle densities of \SIlist{0.5;1.0;1.5;2.0}{\gram\per\centi\metre\cubed} (dotted to solid, respectively) for a range of particle sizes.}
  \label{fig:particle_density}
\end{figure}
