\chapter{Numerics}
\label{chap:numerics}

\section{Developing the numerical scheme}
\label{sec:discretisation}
The goal of this chapter is to develop a numerical scheme that will solve the partial differential equation~\eqref{eq:GDE}.
We will solve this equation without the growth and coagulation terms.
This means that we will drop the index \(i\) from the solution \(q(x, t)\), because we are taking the interaction between particle classes out of consideration.
To simplify matters further, we will write the coefficients for the time derivative and the advection, diffusion and deposition terms as follows:
\begin{align*}
  \alpha &= A_T, \\
  \gamma &= A_A u, \\
  \beta &= A_T D_{\text{eff}}, \\
  \rho &= V_d \Gamma.
\end{align*}
Thus, the equation we wish to solve is as follows,
\begin{equation}
  \label{eq:solvethis}
  \frac{\partial}{\partial t} (\alpha q)= -\frac{\partial}{\partial x} (\gamma q) + \frac{\partial}{\partial x} \left(\beta \frac{\partial q}{\partial x}\right) - \rho q.
\end{equation}
Our numerical scheme will be based on the finite volume method, which divides the spatial domain into intervals.
The solution \(q\) will be estimated by averaging over this volume.
In each time step, the solution will change due to the flux through the cell boundaries, and due to the linear term (the deposition term).
Now let \(Q_j^n\) be the approximation of the solution \(q\) at the \(j\)th grid cell at time step \(n\).
Then this value will approximate the average solution as~\cite{finitevolume}
\begin{equation}
  Q_j^n \approx \frac{1}{\Delta x} \int_{x_{i-1/2}}^{x_{i+1/2}} q(x, t_n)\,dx.
\end{equation}

Integrating~\eqref{eq:solvethis} in space gives
\begin{equation}
  \frac{d}{dt} \int_{x_{i-1/2}}^{x_{i+1/2}} \alpha q(x, t)\,dx = f(q(x_{i-1/2}, t)) - f(q(x_{i+1/2}, t)) - \int_{x_{i-1/2}}^{x_{i+1/2}} \rho q(x, t)\,dx,
\end{equation}
where \(f\) is the flux function depending on the solution \(q\).
We can use this expression to develop a mixed implicit/explicit time marching algorithm.
The mixed implicit/explicit scheme will be based on a modified version of the rectangle rule for integration,
\begin{equation}
  \int_{t^n}^{t^{n+1}} \alpha q(x_j, t)\,dx = \Delta t [(1-\theta) q(x_j, t^n) + \theta q(x_j, t^{n+1})].
\end{equation}
This method, also known as the \(\theta\)-method, allows a parameter \(\theta\) to vary between 0 and 1, where a value of 0 is a fully explicit method, and a value of 1 is fully implicit.
When \(\theta=0.5\), it is known as the Crank-Nicolson method.
Integrating in time from \(t^n\) to \(t^{n+1}\) and rearranging, yields
\begin{equation}
  \label{eq:eqwithflux}
  \begin{aligned}
    \alpha Q_j^{n+1} = \alpha Q_j^{n} + \frac{\Delta t}{\Delta x} \bigg[&(1-\theta)\left(F_{j-1/2}^n - F_{j+1/2}^n - \Delta x \rho Q_j^{n} \right) \\
    &+ \theta \left(F_{j-1/2}^{n+1} - F_{j+1/2}^{n+1} - \Delta x \rho Q_j^{n+1} \right) \bigg].
  \end{aligned}
\end{equation}
Here \(F_{j-1/2}^n\) is an approximation to the average flux through \(x_{j-1/2}\):
\begin{equation}
  F_{j-1/2}^n \approx \frac{1}{\Delta t} \int_{t_n}^{t_{n+1}} f(q(x_{j-1/2}, t))\,dt.
\end{equation}
Depending on the term (advection or diffusion), we will approximate this flux using the values of the neighbouring grid cells: \(Q_{j-1}\) and \(Q_{j}\).
We can estimate this flux based on the values of \(Q_{j-1}^n\) and \(Q_j^n\).
An illustration of the grid point system is shown in Figure~\ref{fig:cluster}.
\begin{figure}[h]
  \centering
  \input{"../Images/TeX/cluster.tex"}
  \caption{Grid-point cluster for the one-dimensional problem.}
  \label{fig:cluster}
\end{figure}

The flux \(f(q)\) for our problem is
\begin{equation}
  f(q_x, q, x) = -\beta (x) q_x + \gamma (x) q.
\end{equation}
For the derivative \(q_x\), we need to make a profile assumption.
The simplest is a piecewise-linear profile, so that the derivatives on the cell edges are well defined (if \(\beta\) is constant, then this reduces to the central difference scheme).
Unlike the diffusion term, the advection term cannot be discretised using central differences, doing so can result in solutions that are not physically realistic~\cite{patankar1980}.
Instead, the discretisation scheme used for the advection term is the upwind-scheme.
In this way, the value of the advective property is equal to the value on the upwind side of the face.\footnote{Note that the upwind face changes depending on whether we are in the inspiration or expiration phase.}
Then, the flux \(F_{j-1/2}^n\) can be defined as
\begin{equation}
  F_{j-1/2}^n = -\beta_{j-1/2} \left(\frac{Q_j - Q_{j-1}}{\Delta x}\right) + \gamma_{j-1/2} Q_{j-1}.
  % F_{j+1/2}^n = -\beta_{j+1/2} \left(\frac{Q_{j+1} - Q_j}{\Delta x}\right) + \gamma_{j+1/2} Q_{j}.
\end{equation}

Expressions for \(F_{j+1/2}^n\), \(F_{j-1/2}^{n+1}\), and \(F_{j+1/2}^{n+1}\) can be found using a similar approach.
Inserting these approximations for the flux into equation~\eqref{eq:eqwithflux} yields (for the inspiration phase, and \(\theta=0\))
\begin{equation}
  \label{eq:discretisation}
  \begin{aligned}
    \alpha Q_j^{n+1} = \alpha Q_j^{n} + \frac{\Delta t}{\Delta x} \bigg[&\left(-\beta_{j-1/2}^n \left(\frac{Q_j^n - Q_{j-1}^n}{\Delta x}\right) + \gamma_{j-1/2}^n Q_{j-1}^n\right) \\
    - &\left(-\beta_{j+1/2}^n \left(\frac{Q_{j+1}^n - Q_j^n}{\Delta x}\right) + \gamma_{j+1/2}^n Q_{j}^n\right) - \Delta x \rho Q_j^{n} \bigg].
  \end{aligned}
\end{equation}
A similar expression can be derived for the arbitrary \(\theta\) and for the expiration phase.
The discretisation equation~\eqref{eq:discretisation} leads to a system of linear equations, which can be put in matrix form (see Section~\ref{sec:matrix}).
The equation describes how interior points depend on their neighbours; what remains to be done is determine equations for the end-points: the boundary conditions.

\section{Boundary conditions}
Boundary conditions specify how the solution behaves on the boundary values (the beginning and the end of the respiratory tract).
There are two main types of boundary conditions that are of our concern: boundary conditions of the first-type (Dirichlet), and boundary conditions of the second-type (Neumann).
Dirichlet boundary conditions specify the value of the solution on the boundary (e.g.\ the inlet concentration is \SI{1}{\micro\gram\per\metre\cubed}).
Neumann boundary conditions specify the value the derivative of the solution on the boundary.
In our case, the boundary condition on the right side (at the alveoli) is a Neumann boundary condition: the concentration gradient is zero at the end of the respiratory tract, meaning that no aerosols travel further than this.

Boundary conditions of the first-type pose no additional problems in the discretisation process because the concentration at the boundary is already known.
The second-type is slightly more involved, because only the flux is known at the boundary.
Consider the right boundary \(j=N\) during the inspiration phase.
The flux \(F_N\) is given, and since \(F_N \approx F_{N+1/2}\), we can insert this into \eqref{eq:eqwithflux} to obtain an equation for \(Q_N\), without requiring a ghost cell \(Q_{N+1}\).

The numerical model allows for a custom specification of boundary conditions.
That is, it is possible to choose the boundary condition type on both sides.
In our case, it is very much necessary that the boundary conditions are able to change.
When applying the model on the Weibel lung geometry, the boundary condition at the trachea changes type when switching from expiration to inspiration or vice versa~\cite{eulerian}.
During inspiration, the trachea boundary condition is of the first-type with a specified concentration of the aerosol; during expiration, the flux is zero.
Unlike the trachea, the boundary condition at the alveoli is always a second-type where the flux is zero.

\section{Matrix form of the discretisation}
\label{sec:matrix}
We obtained a discretisation equation in Section~\ref{sec:discretisation}.
For each time step \(t^n\), there is a system of linear discretisation equations.
These equations can be written in matrix form.
If we denote \(\bm{Q}^n\) as the vector containing the solution at the grid points \(\{x_0, x_1, \ldots\}\) at a time step \(t^n\), then the matrix form the the discretisation reads
\begin{equation}
    \label{eq:theta}
    \bm{Q}^{n+1} = \bm{Q}^{n} + \frac{\Delta t}{\Delta x} [(1-\theta)(\mathbf{A}^n\bm{Q}^{n}+\mathbf{b}^n) + \theta(\mathbf{A}^{n+1}\bm{Q}^{n+1}+\mathbf{b})].
\end{equation}
The system is linear, so it is actually possible to solve this for \(\bm{Q}^{n+1}\) directly, as the derivation below shows:
\begin{align*}
    \bm{Q}^{n+1} &= \bm{Q}^{n} + \frac{\Delta t}{\Delta x} [(1-\theta)(\mathbf{A}^n\bm{Q}^{n}+\mathbf{b}^n) + \theta(\mathbf{A}^{n+1}\bm{Q}^{n+1}+\mathbf{b}^{n+1})], \\
    \bm{Q}^{n+1} - \frac{\Delta t}{\Delta x} \theta \mathbf{A}^{n+1}\bm{Q}^{n+1} &= \bm{Q}^{n} + \frac{\Delta t}{\Delta x}(1-\theta)\mathbf{A}^n\bm{Q}^{n} + \frac{\Delta t}{\Delta x} ((1-\theta)\mathbf{b}^n + \theta \mathbf{b}^{n+1}), \\
    (I - \frac{\Delta t}{\Delta x} \theta \mathbf{A}^{n+1})\bm{Q}^{n+1} &= (I + \frac{\Delta t}{\Delta x}(1-\theta)\mathbf{A}^n)\bm{Q}^{n} + \frac{\Delta t}{\Delta x} ((1-\theta)\mathbf{b}^n + \theta \mathbf{b}^{n+1}), \\
    \bm{Q}^{n+1} &= (I - \frac{\Delta t}{\Delta x} \theta \mathbf{A}^{n+1})^{-1}[(I + \frac{\Delta t}{\Delta x}(1-\theta)\mathbf{A}^n)\bm{Q}^{n} + \frac{\Delta t}{\Delta x} ((1-\theta)\mathbf{b}^n + \theta \mathbf{b}^{n+1})].
\end{align*}
This expression makes it very easy to calculate the concentration for the next time step, both explicitly and implicitly.
For a higher dimensional problem, the computational cost of solving the system implicitly immediately becomes clear, as it requires a costly \(\mathcal{O}(n^3)\) matrix inversion.
In our case, we can use the tridiagonal matrix algorithm to do this in \(\mathcal{O}(n)\).

\section{Consistency, stability and convergence}
In this section we analyse how our numerical method converges to the correct solution as the grid is refined.
In particular, we will look at the stability and consistency of our numerical method, and use these to prove it converges.
A numerical method can be said to converge if the difference between the solution and the numerical approximation tends to zero as the grid is refined.
To analyse the convergence, we introduce the global truncation error at a time \(T=n \Delta t\) as
\[E^n = Q^n - q^n;\]
our goal is to keep \(E^n\) bounded for arbitrary \(n\), by choosing the grid size appropriately.
What it means to be convergent, is formalised in the following definition.
\begin{definition}[Convergence]
  A numerical method \(\mathcal{N}\) is called convergent a time \(T\) in the norm \(||\cdot||\) if
  \begin{equation}
    \lim_{\Delta t \to 0} ||E^n|| = 0.
  \end{equation}
\end{definition}

In general, it is difficult to obtain an analytical expression for the global truncation error as the number of time steps increases.
Instead, we will make use of a result known as the Lax Equivalence Theorem, which states that a consistent approximation to a well-posed initial-value problem is convergent if and only if it is stable.
Before we can define consistency, we need to introduce a concept called the local truncation error, which indicates how much error is introduced at a single time step.
If we let \(\mathcal{N}\) represent our numerical operator mapping the solution from one time step to the next, then the local truncation error is defined as:
\begin{equation}
  \tau^n = \frac{1}{\Delta t} \left[\mathcal{N}(q^n) - q^{n+1}\right].
\end{equation}
\begin{definition}[Consistency]
  A numerical method \(\mathcal{N}\) is called consistent with the differential equation if the local truncation error vanishes as \(\Delta t \to 0\) for all smooth functions \(q(x, t)\) satisfying the differential equation~\cite{finitevolume}.
\end{definition}

Where consistency aims to get a bound on the error introduced in a single time step, stability aims to extend this bound to the global truncation error, thus proving convergence.
To prove that our numerical method is stable, we will use von Neumann stability analysis.
The idea behind this is expanding \(Q_j^n\) as a Fourier series, and proving that \(\hat{Q}^n\) is bounded.
Subsequently, it is possible to obtain a bound on \(Q_j^n\) using Parseval's relation.
Parseval's relation requires using the 2-norm, instead of say, the \(\infty\)-norm.
For completeness, we will give the definition of stability and Lax's Equivalence Theorem.
\begin{definition}[Stability]
  A numerical method \(\mathcal{N}\) is called stable in the norm \(||\cdot||\) if for some \(C>0\),
  \begin{equation}
    ||\mathcal{N}_j^n|| \leq C
  \end{equation}
  for all \(n\) and \(j\) such that \(0 \leq nj \leq T\).
\end{definition}
\begin{theorem}[Lax Equivalence Theorem~\cite{trefethen}]
  Let \(\mathcal{N}\) be a consistent approximation to a well-posed linear initial-value problem.
  Then \(\mathcal{N}\) is convergent if and only if it is stable.
\end{theorem}
Using the definitions from the previous section, we can now go about proving consistency and stability for our numerical method.
To simplify matters, we will assume that our differential equation contains constant coefficients.
In addition, we will do the derivation for the explicit case, and show how this can be extended to the mixed explicit-implicit scheme.

\subsection{Consistency}
\label{subsec:consistency}
Applying our numerical method~\eqref{eq:discretisation} to the true solution gives the local truncation error
\begin{equation}
  \label{eq:localtruncstart}
  \tau^n = \frac{1}{\Delta t} \left(\frac{1}{\alpha}\left[\alpha q_j + \frac{\Delta t}{\Delta x}\left(\beta \frac{q_{j+1} - 2 q_j + q_{j-1}}{\Delta x} + \gamma (q_{j-1} - q_j) - \Delta x \rho q_j\right)\right] - q_j^{n+1}\right)
\end{equation}
where we have assumed constant coefficients for simplicity.
The polynomial approximation of \(q_{j-1}^n\), \(q_{j+1}^n\) and \(q_j^{n+1}\) about \(q_j^n\) gives the following
\begin{align}
  q_{j+1}^n &= q_j + \Delta x q_x + \frac{1}{2} \Delta x^2 q_{xx} + \mathcal{O}(\Delta x^3), \\
  q_{j-1}^n &= q_j - \Delta x q_x + \frac{1}{2} \Delta x^2 q_{xx} + \mathcal{O}(\Delta x^3), \\
  q_j^{n+1} &= q_j + \Delta t q_t + \mathcal{O}(\Delta t^2).
\end{align}
If we insert this into our expression for the local truncation error, we obtain the following
\begin{equation}
  \begin{aligned}
    \tau^n &= \frac{1}{\Delta t} \left(\frac{1}{\alpha}\left[\alpha q_j + \frac{\Delta t}{\Delta x}\left(\beta \frac{\Delta x^2 q_{xx} + \mathcal{O}(\Delta x^3)}{\Delta x} - \gamma \Delta x q_x + \mathcal{O}(\Delta x^2) - \Delta x \rho q_j\right)\right] - q_j^{n+1}\right), \\
    \tau^n &= \frac{1}{\Delta t} \left(\frac{1}{\alpha}\left[\alpha q_j + \Delta t\left(\underbrace{\beta q_{xx} - \gamma q_x - \rho q}_{\alpha q_t} + \mathcal{O}(\Delta x) \right)\right] - q_j^n - \Delta t q_t +\mathcal{O}(\Delta t^2)\right), \\
    \tau^n &= \frac{1}{\Delta t} \left(\Delta t \mathcal{O}(\Delta x) + \mathcal{O}(\Delta t^2)\right), \\
    \tau^n &= \mathcal{O}(\Delta x) + \mathcal{O}(\Delta t).
  \end{aligned}
\end{equation}
Here the error is dominated by both a \(\Delta t\) and \(\Delta x\) term, so we see that the method is first-order accurate.\footnote{In fact, expanding up to \(\mathcal{O}(\Delta x^4)\) shows that the diffusion term is second-order accurate.}
This proves that our numerical method is consistent with the differential equation.

For the mixed explicit-implicit case, the term in~\eqref{eq:localtruncstart} will be split into an explicit \(1-\theta\) part, and an implicit \(\theta\) part.
In this case, the \(q_j^{n+1}\) would be split as such as well, but only the part pertaining to the explicit solution would be Taylor expanded around \(q_j^{n}\).
Similarly, the implicit term in brackets would be Taylor expanded about \(q_j^{n+1}\).
This results in first-order accuracy, as before.

\subsection{Stability}
In this section we will prove our numerical method is stable using von Neumann stability analysis.
We follow the approach stipulated in~\textcite{finitevolume}, writing \(Q_j^n\) as follows
\begin{equation}
  \label{eq:arbitrarywavenumber}
  Q_j^n = e^{i \xi j \Delta x}.
\end{equation}
Rewriting our finite difference scheme and grouping by like terms, we obtain
\begin{equation}
  \begin{aligned}
    \alpha Q_j^{n+1} &= \alpha Q_j + \frac{\Delta t}{\Delta x} \left(\beta \frac{Q_{j+1} - 2Q_j + Q_{j-1}}{\Delta x} + \gamma (Q_{j-1} - Q_j) - \Delta x \rho Q_j\right), \\
    &= \left(\alpha - \frac{2 \beta \Delta t}{\Delta x^2} - \frac{\Delta t}{\Delta x} \gamma - \Delta t \rho\right) Q_j + \frac{\Delta t}{\Delta x^2} \beta Q_{j+1} + \left(\frac{\Delta t}{\Delta x} \gamma + \frac{\Delta t}{\Delta x^2} \beta\right) Q_{j-1}.
  \end{aligned}
\end{equation}
Substituting the coefficients of \(Q_j\), \(Q_{j+1}\) and \(Q_{j-1}\) by \(A\), \(B\) and \(C\), respectively, and applying~\eqref{eq:arbitrarywavenumber} to our finite difference scheme yields
\begin{equation}
  \begin{aligned}
    \alpha Q_j^{n+1} &= A Q_j + B Q_{j+1} + C Q_{j-1}, \\
    &= A e^{i \xi j \Delta x} + B e^{i \xi (j+1) \Delta x} + C e^{i \xi (j-1) \Delta x}, \\
    &= \left(A + B e^{i \xi \Delta x} + C e^{-i \xi \Delta x}\right) e^{i \xi j \Delta x}.
  \end{aligned}
\end{equation}
Then, the amplification factor becomes
\begin{equation}
  \label{eq:ampfactor}
  g(\xi, \Delta x, \Delta t) = \frac{1}{\alpha} \left[A + B e^{i \xi \Delta x} + C e^{-i \xi \Delta x}\right].
\end{equation}
Here, if \(|g(\xi, \Delta x, \Delta t)| \leq 1 \) for all \(\xi\) then the numerical scheme is stable.
Absolute stability is obtained if the amplification factor is strictly less than 1.
Because the amplification factor consists of multiple terms, it is not trivial to see when the inequality holds.
We can make progress by invoking the triangle inequality, which gives
\begin{equation}
  |g(\xi, \Delta x, \Delta t)| = \left|\frac{1}{\alpha} \left[A + B e^{i \xi \Delta x} + C e^{-i \xi \Delta x}\right]\right| \leq \frac{1}{\alpha} \left[|A| + |B| + |C|\right].
\end{equation}
By looking at \(A\), \(B\), and \(C\), it seems plausible that the amplification factor can become less than unity depending on the input parameters and grid size.
In particular, if we look at \(B=\frac{\Delta t}{\Delta x^2} \beta\), we see that \(\Delta t\) must be of the same order as \(\Delta x^2\).
We require this because if they are of the same order, then \(B\to\infty\), rendering our numerical scheme unstable.

Invoking the triangle inequality does not guarantee that we can prove that the numerical scheme is stable.
In fact, it could be the case that our upper limit (using the triangle inequality) is always larger than one, but that the numerical scheme is still stable for some \(\Delta x\) and \(\Delta t\).
A better approach is plotting the function \(g\) and inspecting it visually.
It can be difficult to visualise functions \(f:\mathbb{C}\to\mathbb{C}\) but there are some tools\footnote{\url{https://people.ucsc.edu/\~wbolden/complex/\#(a-(2bt/x\%5E2)-(t/x)g-tp)+(t/x\%5E2)be\%5E(iz)+((t/x)g+(t/x\%5E2)b)e\%5E(-iz)}} that accomplish this.
We can also parameterise the domain and plot it on an Argand diagram, this is done in Figure~\ref{fig:argand}.
\begin{figure}
  \centering
  \input{"../Images/Graphs/stability.tex"}
  \caption{%
  Argand diagram showing how \(g\) maps the domain \(\mathbb{R}\) to \(\mathbb{C}\).
  Clearly, for these particular values of \(\Delta x\) and \(\Delta t\), the image of \(g\) is contained in the unit circle.}
  \label{fig:argand}
\end{figure}

For the mixed explicit-implicit case we can substitute \(q_j^n = g^n e^{i \xi j \Delta x}\) into our finite difference scheme, and we obtain
\begin{equation}
  g(\xi, \Delta x, \Delta t) = \frac{\frac{1}{\alpha}\left(A + B e^{i \xi \Delta x} + C e^{-i \xi \Delta x}\right)(1-\theta)}{1 - \frac{1}{\alpha}\left(A + B e^{i \xi \Delta x} + C e^{-i \xi \Delta x}\right)\theta}.
\end{equation}
If \(\theta=0\), then this reduces to our familiar expression~\eqref{eq:ampfactor}.
Note that this expression is not valid for \(\theta=1\), which results in an amplification factor of zero, regardless of the grid size and \(\xi\).

We know from Section~\ref{subsec:consistency} that our numerical approximation is consistent with the boundary value problem, and in this section we have seen that it is also stable.
This proves, using Lax' equivalence theorem, that our numerical approximation is convergent.

\section{Miscellaneous observations}
In this section a few comments are made to give a more complete view of how our differential equation is discretised, as well as some considerations made in the process.
\subsection{The CFL condition}
The CFL condition, named after Courant, Friedrichs and Lewy, is a necessary condition for a finite volume scheme if we expect it to be convergent.
It requires that the numerical domain of dependence contains the true domain of dependence.
In other words, for a given point \(Q_j^{n+1}\), the numerical method must include every other point that is able to affect it.
For the fully implicit method, the CFL condition is always satisfied.
The reason for this is that determining the approximation at \(t^{n+1}\) requires knowledge of all values at \(t^n\), meaning that no matter the grid spacing or time step, we are always using the entire physical domain of dependence~\cite{CFLstackexchange}.
For the explicit upwind method without diffusion, the CFL condition is
\begin{equation}
  \left|\frac{\gamma \Delta t}{\Delta x}\right| \leq 1.
\end{equation}
If we were solving the equation explicitly, our grid size would have to conform to this criterion.

\subsection{Upwind vs central difference}
Usually the advection term is not discretised using a central difference scheme, as doing so can result in solutions that are not physically realistic~\cite{patankar1980}.
The benefit of using a central difference scheme for the advection term, is that it is second-order accurate.
This allows for a larger grid size while keeping the numerical scheme consistent, reducing computational time.
In general, the order of accuracy is only as good as the worst order of accuracy in the approximation.
Since we have both a diffusion and advection term, the order of accuracy of our method will be equivalent to the order of accuracy of the worst of the two.
In our case, the advection term is first-order accurate, but since we are only trying to solve the problem for a few breathing cycles, pursuing higher order accuracy is unnecessary, because computational time is not an issue.

\subsection{Nonuniform grid spacing}
\label{subsubsec:gridspacing}
Instead of using a grid with uniform grid spacing, we can employ a so called nonuniform grid.
This means that the distances \((\delta x)_{j-1/2}\) and \((\delta x)_{j+1/2}\) are not necessarily equal.
Usually, nonuniform grid spacing is used to deploy computing power more efficiently.
In places where \(q\) varies steeply with \(x\) the grid should be finer than where \(q\) changes rather slowly with \(x\).

These concerns are a valid reason to use a nonuniform grid, but in our case there is another reason.
This stems from the fact that the first generation is a lot longer than say, the 21st.
If we use a nonuniform grid, it is possible distribute the grid points over the generations equally.
This is especially useful when using fewer grid points, because in that case it is possible that final generations have only a single grid point, and if this grid point is in the first 80\% of the airway, then the impaction velocity over the whole generation is zero (see Section~\ref{subsubsec:deposition}).

\subsection{Numerical calculation of the velocity field}
\label{subsec:numericalvelocity}
In Section~\ref{subsec:velocity} we derived an expression for the velocity profile.
Here, we will briefly touch on how to numerically evaluate this profile.
By looking at equation~\eqref{eq:velocityprofile}, we see that we need to be able to numerically evaluate the derivative \(\frac{\partial}{\partial t} A_T\), and the spatial integral over this derivative.
The first thing to notice is that \(\frac{\partial}{\partial t} A_T=0\) for generations \(<16\), simplifying the equation to a simple mass balance.
If we evaluate this expression for the later generations, then we can start to iterate over \(x\) and \(t\) to derive the entire velocity profile.

Fortunately, it simply suffices to use a central difference rule for the derivative, and any numerical integration method such as the trapezoidal rule.
There exists a central difference function \lstinline{numpy.gradient} that calculates the central difference for us, and uses forward and backward differences for the two endpoints.
Similarly, the trapezoidal rule is implemented in \lstinline{scipy.integrate.trapz}.
We can then choose \(a=0\) or any other \(a < \text{generation }16\) and start the iteration process.
