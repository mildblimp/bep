\chapter{Model verification}
\label{chap:modelverification}

As in any numerical study, it is important to verify that the numerical results are physically accurate.
In addition to standard sanity checks such as interchanging the boundaries or using physical common sense, another way to do this is by comparing numerical results to experimental results.
In our case, this is not possible, but it is possible to compare the results to those of \textcite{eulerian}.
Additionally, it is also possible to apply the model to a problem that has a known analytical solution.
This increases (by induction) the confidence in our solution for the extended problems (but it is by no means certain that the solutions for the extended problems are correct!).

\section{Comparison with analytical solution}
\label{subsec:analytical}
We begin our verification with the last approach: we test our model on the 1-dimensional advection-diffusion equation, with a first-type boundary condition on the left side, \(q(0, t)=2\), and a second-type boundary condition on the right \(q_x(3,t)=0\).
The governing equation reads
\begin{equation}
  \frac{\partial q}{\partial t} = -u \frac{\partial q}{\partial x} + D \frac{\partial^2 q}{\partial x^2},
\end{equation}
where the initial condition is \(q(x,0)=0\).

The analytical solution to this equation is known, and has been compiled (along with solutions to similar problems) by~\textcite{analytical}.
The comparison between their analytical solution and the solution of our model is shown in Figure~\ref{fig:analytical}.
\begin{figure}[h]
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/peclet-0.003.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{Péclet = 0.003}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/peclet-0.03.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{Péclet = 0.03}
  \end{subfigure}
  \\
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/peclet-0.3.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{Péclet = 0.3}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/peclet-3.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{Péclet = 3}
  \end{subfigure}
  \caption{The solutions to a 1-dimensional advection-diffusion equation for different Péclet numbers. The dotted lines represent the numerical solutions, and the solid lines represent the analytical solutions.}
  \label{fig:analytical}
\end{figure}
It is easy to see that for the range of different Péclet numbers\footnote{The Péclet number is defined as the ratio of the rate of advection to the rate of diffusion of a physical quantity, or \(\text{Pe}=Lu/D\).}, the numerical model holds up well.

In addition to the non-stationary problem, the solution for the stationary problem was also compared with the numerical model.
The exact solution to the stationary problem is known, it is
\begin{equation}
  \label{eq:exactsteadystate}
  q(x) = q(0) + [q(L) - q(0)] \frac{\exp{(\text{Pe}\, x / L)}-1}{\exp{(\text{Pe}\,)}-1},
\end{equation}
where \(\text{Pe}\) is the Péclet number and the boundary conditions are both of the Dirichlet type.
A comparison of this solution with that of the numerical model at large times is shown in Appendix~\ref{app:longtimes}.

\section{Comparison with literature}
In this section we concern ourselves with comparing our results to those of~\textcite{eulerian}.
We will primarily compare the deposition fractions for a range of different particle sizes, as they do not specify the aerosol concentration for a range of different time steps.
Before the literature comparison, we will look at the velocity profile and concentration profile ourselves, to see if they are physically viable.

\subsection{Velocity profile}
\label{subsec:velocityprofile}
As explained in Section~\ref{subsec:velocity}, we first have to calculate the velocity field before we can solve the GDE~\eqref{eq:GDE}.
The velocity is of course closely related to the breathing function, and the tidal volume.
In our case we have a symmetric breathing function, and we use a tidal volume of \SI{1000}{\milli\litre}.

From the inlet velocity, we can calculate the total velocity profile over the entire RT.
This profile will be a step function in the non-alveolated generations, because the velocity over a generation is assumed to be constant (due to the continuity equation and the fact that the airway diameter is constant over a generation).
The velocity profile over a range of different time steps is shown in Figure~\ref{fig:multi_velocity}.
From the velocity profile and the (changing) airway diameter, it is possible to calculate Reynolds number as well.
This is done in Figure~\ref{fig:multi_reynolds}.
Likewise, this is also a stepwise function due to the stepwise properties of generation values.
\begin{figure}[h]
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/multi_velocity.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{Velocity profile in the RT.}
    \label{fig:multi_velocity}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/multi_reynolds.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{Reynolds number in the RT.}
    \label{fig:multi_reynolds}
  \end{subfigure}
  \caption{%
  The velocity profile and Reynolds number plotted during the first second of a symmetric breathing cycle of period \SI{4}{\second}.
  At \(t=\SI{1}{\second}\) the velocity reaches its peak.
  The dotted lines in these and subsequent figures signify the start of a new generation.
  Naturally, the tidal volume and breathing function have a large impact on the velocity profile.
  }
\end{figure}

It is clear that the velocity is highest in the third generation, and approaches zero for the later generations.
This is because the cross sectional surface area decreases up until the third generation, and after this increases monotonically.
Throughout the cycle, Reynolds number varies between 1 and around 2500, which just about reaches the critical value that marks the change into the turbulent regime.
In the RT, Reynolds number decreases monotonically due to the decreasing velocity, and decreasing characteristic linear dimension (diameter).
In the later generations the velocity is almost negligible, because most of the air moves into the alveolar sacs instead of continuing through the conducting part of the RT.
This also conforms to our expectation, and for these reasons the velocity profile looks to be physically acceptable.
What remains to be seen is whether the concentration profile is acceptable as well.

\subsection{Concentration profile}
By solving the aerosol general dynamic equation~\eqref{eq:GDE}, we are able to determine the concentration \(q(x, t)\) for the entirety of the respiratory tract and also at every time step \(t\).
We thus obtain a complete description of the aerosol dynamics.
The concentration profile depends heavily on the particle size.
If the particles are small, then they are able to reach the transitional bronchioles (generation \(\geq 17\)), and even the alveolar ducts (generation \(\geq 20\)).
The larger particles however, do not penetrate as deeply, and because of this they also leave the RT quicker than the smaller particles, which can stay in the RT even at the end of the expiration phase (depending on the tidal volume).

Figure~\ref{fig:multi_concentration} shows the concentration profile at different time steps for particles of size \(d=\SI{0.01}{\micro\metre}\).
The different time steps correspond to the first second of a symmetric breathing cycle of period \SI{4}{\second}, in \SI{0.1}{\second} intervals.
\begin{figure}[h]
  \centering
  \input{"../Images/Graphs/multi_concentration.tex"}
  \caption{%
  The concentration profile of an aerosol of diameter \SI{0.01}{\micro\metre} plotted during the first second of a symmetric breathing cycle of period \SI{4}{\second}.
  Starting at 0, the concentration increases with each time step (in \SI{0.1}{\second} intervals), until it stabilises.
  Note that the total time span is the first half of the inspiration phase.
  }
  \label{fig:multi_concentration}
\end{figure}
It is remarkable that the concentration profile very quickly approaches zero when the expiration phase starts.
And because the concentration is zero at the end of expiration, a steady breathing pattern is established instantaneously.
This seems counter-intuitive, especially if you pay attention to your own breathing.
If you breathe in deeply, it can take quite a while before all the air has exited your lungs.
However, the aerosols do not penetrate the lungs so deeply (compared to the much smaller \ce{N2}, \ce{O2}, and \ce{CO2} molecules), which is why the aerosols exit the lungs much quicker.
For a study specifically describing the effect of these smaller molecules, consult~\cite{dejongh}.

In principle, our model accepts particle sizes in the range of \SI{0.1}{\nano\metre}, but applying our model on the constituent gasses of air (with those sizes) results in nonphysical results.
This is because the concentration of these gasses is much higher, at which point our original partial differential equation does not describe our particles any longer.
Indeed, the deposition will be much lower than described by the general aerosol equation.

\subsection{Deposition fraction}
\label{subsec:depfrac}
The goal of our programme is to calculate the deposition fraction for different classes of aerosol particles.
The difference in particles that leads to the biggest change in the deposition fraction is the particle diameter.
Indeed, smaller particles will experience strong diffusion, whereas the dominant deposition mechanism for bigger particles is gravitational settling or impaction.

In Section~\ref{subsec:analytical} we tested our model on a known analytical solution.
This is not enough however, because this gives us confidence only for a particular situation.
Our models aims to determine the deposition ratio of aerosols of different sizes, and we can compare our results to~\cite{eulerian}.
The literature study compares the deposition ratio for particles of different sizes, ranging from \SIrange{0.01}{10}{\micro\metre} in diameter.
The lung conditions are a tidal volume of \SI{1000}{{\milli\litre}} and a period of respiration of \SI{4}{\second}.
Their results, along with ours are shown in Figure~\ref{fig:depositionfraction}.
\begin{figure}[h]
  \centering
  \input{"../Images/Graphs/deposition_fraction.tex"}
  \caption{%
  Comparison of the calculated deposition fraction with literature~\cite{eulerian} as a function of particle diameter.
  The dotted line shows our results.}
  \label{fig:depositionfraction}
\end{figure}

The results match very closely, even though we do not take into account particle-particle and gas-to-particle interactions (coagulation and growth).
The offset cannot entirely be explained by the neglection of these two terms.
Including these two interactions increases or decreases the particle diameter during the simulation process either by growth or coagulation.
Because of the high humidity in the lungs, particles are expected to grow significantly, which increases their effective particle diameter.
This would shift our results to the left, but this is not entirely explained by Figure~\ref{fig:depositionfraction}.

More likely, the offset is explained by a difference in starting conditions.
For example, the particle density influences the curve as well.
In this case, the particle density is \SI{1}{\gram\per\centi\metre\cubed}, but if we increase this two or threefold, the deposition fraction increases for all particle sizes.
The larger particles (\SI{>1}{\micro\metre}) are especially affected, however, as their relaxation time is much higher (which influences impaction), and the particle relaxation time depends non-linearly on the particle diameter.
It is a bit unclear what the exact starting conditions of the literature study~\cite{eulerian} are.
They produce a number of graphs with seemingly different deposition fraction curves, some of which look more similar to our results in Figure~\ref{fig:depositionfraction}.

Instead of calculating the total deposition fraction, it is also possible to calculate local deposition fractions, for example the deposition fraction per generation.
This is done in Figure~\ref{fig:local_deposition}, where we also compare our results to those compiled by~\cite{eulerian}.
Here, we have calculated the local deposition fraction for particles with diameters of~\SIlist{0.01;1}{\micro\metre}.

The results for the smaller particles match closely with literature, as can be seen in Figure~\ref{subfig:local_small}.
The model seems to overestimate the deposition fraction for the larger class of particles from generation 20 onward.
This could be due to an overestimation of the diffusion velocity, which especially affects generations with smaller diameters.
Here, the flexibility of our programme starts to be appreciated, because we can turn off particular deposition mechanisms to see which is the cause of the irregularity.

In this case, gravitational sedimentation seems to be culpable for the difference in Figure~\ref{subfig:local_big}.
This does not necessarily cast doubt on the validity of our model; the literature study is slightly vague in how they obtained the gravity angles for their model.
Although they specified that they obtained the angles from Weibel's model ``A'', this model neither specifies individual branching angles, nor the angles of inclination with respect to the horizontal (the gravity angles)~\cite{respiratorydrugdelivery}.
If the gravity angles are smaller in the later generations, the results would look more similar.
Nevertheless, the preceding comparisons give us the confidence that our model performs well under a range of different conditions, and we will use this to perform a parametric study in Chapter~\ref{chap:results}.
\begin{figure}[h]
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/local_deposition_0.01e-6.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{\(d=\SI{0.01}{\micro\metre}\)}
    \label{subfig:local_small}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{"../Images/Graphs/local_deposition_1e-6.tex"}
    \vspace{\subfigcaptionspacing}
    \caption{\(d=\SI{1}{\micro\metre}\)}
    \label{subfig:local_big}
  \end{subfigure}
  \caption{%
  Comparison of the calculated local deposition fraction with literature~\cite{eulerian} for particles of size~\SIlist{0.01;1}{\micro\metre}
  The dotted line shows our results.}
  \label{fig:local_deposition}
\end{figure}
