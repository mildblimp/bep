\chapter{Theory}
\label{chap:theory}
The goal of this chapter is to introduce the underlying theory behind calculating aerosol deposition in the lungs.
We first introduce the lung model, after which we will discuss the behaviour of aerosol dynamics.
Calculations of deposition fractions and the velocity field is discussed next, and this chapter concludes by specifying various constants used in the model, and introducing the medical terminology required to understand the parametric studies in Chapter~\ref{chap:results}.

\section{Lung model}
\label{subsec:lungmodel}
The model used in this research is the Weibel model ``A''~\cite{weibel}, which describes the lungs as a series of bifurcations.
Lungs are composed of a series of tubular branches, starting with the trachea (windpipe), and ending with the alveoli.
The Weibel model has 23 generations: the trachea (generation 0) splits up into two bronchi (generation 1), and this process continues all the way up to generation 23 (alveoli).
The model is symmetric, and therefore easy to describe using a 1-dimensional model.

A schematic of the model is shown in Figure~\ref{fig:weibelimage}.
The model consists of purely conducting airways for the first 16 generations.
Generation 16 marks the start of the transitional section, and from generation 16 onward, the lungs become alveolated and contract and expand due to breathing mechanics.
The size of the later generations is very small, but because the model bifurcates at every generation, the number of alveolar sacs in the last generation is \(8\,388\,608\).
This also means that the volume and surface area of the lungs greatly increases towards the end.
This is why this type of model is also known as the ``trumpet'' model, and an illustration of this can be found in Figure~\ref{fig:trumpet}.
\begin{figure}[h]
  \centering
  \input{"../Images/TeX/schematics.tex"}
  \caption{Schematics of hierarchy by Weibel's model. The bifurcations continue until the 23rd generation.}
  \label{fig:weibelimage}
\end{figure}

The parameters that define the Weibel model are the generation length, the generation diameter, the branching angles, the gravity angles, and the number of airways per generation.
Both the generation length and the diameter decrease with the generation number, while the number of airways per generation obviously increases.
The branching angle is the angle that the next generation makes with the previous, and it varies from \ang{20} to \ang{30} for the first generations, and is around \ang{45} for the later generations.
The gravity angle is the angle the airway makes with the gravity vector (when standing upright) and ranges from about \ang{40} to \ang{60}.
In this research, the data for the Weibel model ``A'' is obtained from~\cite{weibeldata} and is included in Appendix~\ref{app:data}.

Note that this model does not account for the nose or mouth and its accompanying structure.
If the nose or mouth need to be modelled, it is possible to add a generation before generation 0, as has been done in~\cite{eulerian} for example.
\begin{figure}[h]
  \centering
  \makebox[0pt]{\includegraphics[scale=1]{"../Images/trumpet-model.jpg"}}
  \caption{Illustration of the one-dimensional ``trumpet'' model. Image taken from~\cite{compilation}.}
  \label{fig:trumpet}
\end{figure}

\section{Aerosol dynamics}
\label{subsec:dynamics}
Aerosol dynamics are governed by a number of physical processes, and we can mathematically model these to get a description of the complete particle distribution.
The models describing these dynamics can be split into three different groups: particle movement, particle-surface interaction, and particle-particle interaction.
The main processes describing particle movement are Brownian diffusion and advection, which describe how particles move en masse.
Brownian diffusion also describes how smells disperse through the air, whereas advection is usually associated with wind.
Apart from these two main processes, particle movement can also arise from electric fields or temperature gradients.

The second group, particle-surface interaction, models the situation in which particles either deposit on a surface, or release from it.
In this research, we are particularly concerned with particle deposition on the walls of the lungs.

The third group is comprised of processes with particle-particle interactions.
This is primarily coagulation, where aerosols collide with each other to form larger aerosols (or split to form smaller ones).
Other processes included are growth laws, in which other particles attach to the aerosol in a manner known as gas-to-particle interaction.

These groups of processes can be summarised in an equation known as the Aerosol General Dynamics Equation (GDE).
This equation describes how aerosols behave as a group.
The one-dimensional form of the equation reads~\cite{eulerian}
\begin{equation}
  \label{eq:GDE}
  \begin{aligned}
    \frac{\partial}{\partial t} (A_T q_i)= &-\frac{\partial}{\partial x} (A_Au q_i) + \frac{\partial}{\partial x} \left(A_TD_{\text{eff}} \frac{\partial q_i}{\partial x}\right) - V_d \Gamma q_i \\
    &+ \left(\frac{\partial}{\partial t} (A_Tq_i)\right)_{\text{coagulation}} + \left(\frac{\partial}{\partial t} (A_Tq_i)\right)_{\text{growth}}.
  \end{aligned}
\end{equation}
Our goal is to determine \(q_i(x, t)\) for all \(x\) (the position in the respiratory tract) and \(t\) (the time coordinate).
The index \(i\) iterates over the different particle sizes, making it possible to model particles of different diameters, while including particle-particle interaction for particles of different sizes.
The equation also includes \(A_A\) and \(A_T\), which are the airway cross-sectional areas (of all airways).
The difference between the two terms is that \(A_T\) includes a time dependency for the alveolated generations, which vary in diameter with time.

The first term on the right side of~\eqref{eq:GDE} is the advection term.
This describes how particles behave under an external flow field.
In our case, this external field is the velocity of the air \(u\) traveling through our lungs due to breathing.
It is the dominant force for aerosol migration, and it reverses sign every breathing cycle.
Advection is modelled to occur only in the conducting part of the airway, which is why \(A_A\) is used for the advection term (advection does not occur through the alveolar part).

The second term is the Brownian diffusion term.
This arises from fluctuating forces exerted by surrounding molecules, and the random motion results in a net movement of aerosols against the direction of the concentration gradient.
The strength of the diffusion is described by the effective diffusion coefficient: \(D_\text{eff}\), and it also depends on the particle size.

The linear term \(- V_d \Gamma q_i\) describes the deposition of particles into the lungs.
Deposition is an interaction in which particles collide with the surface (the airway walls) and are absorbed.
This is the term we are most interested in, and it depends on the deposition velocity \(V_d\), which will be described in Section~\ref{subsubsec:deposition}.
The parameter \(\Gamma=n \pi d_T\) is the wetted perimeter, which depends on the number of airways per generation \(n\) and the airway diameter \(d_T\).
In essence, the amount of deposition depends on the perimeter of the airway, the deposition velocity and the local concentration of aerosols.

The coagulation term describes particles changing their respective particle size group by combining (or splitting) with other particles.
It results from particle-particle interactions and is one of the ways the particle sizes are interdependent.
Particle coagulation also leads to a reduction in the overall concentration of particles.

Unlike coagulation, which modifies the size distribution of aerosols, gas-to-particle conversion increases the aerosol mass concentration, while also affecting the size distribution.
The latter can be modelled by so called ``growth'' laws and allows for calculating the changing size distribution function.
There are a number of growth laws, for example: molecular bombardment, surface reaction, droplet-phase reaction.
The growth law can be calculated using Mason's theory~\cite{mason}.
This process, along with particle coagulation, will not be considered in this research.

\subsection{Deposition velocity}
\label{subsubsec:deposition}
The deposition velocity is the velocity of the aerosols that deposit on the surface of the airways.
It is comprised of three separate effects: gravitational settling, Brownian diffusion, and impaction due to curves in the lung bronchi and bronchioles.
This is explained in detail in~\cite{eulerian} and is included here for completion.
\paragraph{Gravitational settling}
The simplest of the three is the deposition due to gravitational settling.
Like all objects in a gravitational field, aerosols are also affected by it, and it is written as follows:
\begin{equation}
  \label{eq:gravitationalsettling}
  V_s = u_s \sin{\theta}.
\end{equation}
Here the velocity \(u_s\) is determined from Stokes' law, and the angle \(\theta\) is the angle the airways make with the gravity vector.
We assume that the particle terminal velocity is reached instantly, and then the velocity \(u_s\) follows from a balance of forces of the gravitational force and the Stokes' drag force~\cite{stokeslaw}
\begin{align}
  0 &= mg - 3 \pi \mu_g d u_s \\
  \implies u_s &= \frac{\rho d^2 g}{18 \mu_g},
\end{align}
where we have assumed that the aerosols are spherical.
In this equation, \(\rho\) is the particle density, \(d\) its diameter, \(g\) is the gravitational field strength, and \(\mu_g\) the fluid viscosity.
This expression does not yet account for particle slipping, but this is easily rectified by multiplying the expression by the Cunningham correction factor, which accounts for slip effects for small particles~\cite{cunningham}.

\paragraph{Brownian diffusion}
The deposition velocity arising from Brownian diffusion can be found from mass transfer theory, and depends on Sherwood's number in the following manner~\cite{datacompanion},
\begin{equation}
  V_d = \frac{D_B Sh}{d_T}.
\end{equation}
Here the Brownian diffusion constant \(D_B\) is given by the Stokes-Einstein equation~\cite{stokeseinstein}, found by Einstein and Sutherland independently all the way back in~\citeyear{stokeseinstein},
\begin{equation}
  D_B = \frac{k_B T_g C_c}{3 \pi \mu_g d}.
\end{equation}
Here, \(k_B\) is Boltzmann's constant, \(T_g\) is the temperature of the fluid (air), and \(C_c\) is Cunningham's correction factor; the other constants have already been introduced.
Sherwood's number \(Sh\) is found using an empirical relation for circular ducts.
We use the same source for the empirical relation as~\cite{eulerian}; however, they copied it incorrectly.
The correct expression for Sherwood's number is shown below~\cite{sherwoodemperical}
\begin{equation}
  \label{eq:sherwood}
  Sh =
  \begin{cases}
    1.077 (x^*)^{-1/3} -0.7 & \text{for } x^* \leq 0.01, \\
    3.657 + 6.874(10^3 x^*)^{-0.488} \exp{(-57.2 x^*)} & \text{for } x^* > 0.01.
  \end{cases}
\end{equation}
Here, the value \(x^*\) is the dimensionless length measured from the beginning of the airway (not the beginning of the respiratory tract).
The dimensionless length depends on Reynolds and Schmidt's numbers in the following manner:
\begin{equation}
  x^* = \frac{x}{d_T Re Sc}
\end{equation}
Reynolds number is given by the standard expression \(Re = \rho u d_T / \mu\).
On the other hand, Schmidt's number is given by \(\mu / \rho D_B\).
% This expression is valid for low Reynolds numbers, which holds for the bronchioles and smaller airways by~\cite[p.~42]{weibeldata}.

\paragraph{Impaction}
Finally, the last component of the deposition velocity stems from the fact that particles may not be able to follow the fluid path due to their inertia.
This effect is greatest for large (\SI{>1}{\micro\metre}) particles~\cite{aerosoldynamics}.
Due to the different branching angles \(\phi\) in the lungs, the impaction velocity is significant and is given by.
\begin{equation}
  V_i =
  \begin{cases}
    0 & \text{(over first 80\% of the airway length),} \\
    Stk\,u\phi d_T / 0.2L & \text{(over last 20\% of the airway length),}
  \end{cases}
\end{equation}
where \(Stk\) is Stokes number, and \(L\) is the branch length.
This expression is based on a formal analysis (instead of empirical modelling) and is described in detail in~\cite{eulerian}.

\section{Derivation of the velocity field}
\label{subsec:velocity}
To solve the GDE~\eqref{eq:GDE} we need to solve for the velocity field \(u(x,t)\).
This is done by solving the continuity equation
\begin{equation}
  \frac{\partial \rho}{\partial t} + \frac{\partial (\rho u)}{\partial x} = 0.
\end{equation}
Before deriving the continuity equation specific to our problem, we need to define how the lung volume varies with time.
This is necessary because our cross sectional area \(A_T\) varies with time, according to the lung volume.

Inspiration and expiration (inhalation and exhalation) can be modelled by a simple function \(f(t)\) that varies from -1 to +1.
For our purposes, we will use a sinusoidal function with a breathing rate of 12 breaths per minute, which is the normal respiratory rate for an adult~\cite{breathingrate}.
The lung volume is then defined as follows,
\begin{equation}
  V_L = \left(V_\text{FRC} + \frac{V_T}{2}\right) + \frac{V_T}{2}f(t).
\end{equation}
It consists of a constant part, the functional residual capacity \(V_\text{FRC}\), and a varying part that depends on the tidal volume \(V_T\), and the breathing function \(f(t)\).
These terms will be explained in Section~\ref{sec:breathingpatterns}, and in particular, Figure~\ref{fig:lungvolumes}.
The cross sectional area \(A_T\), or rather, the time dependent diameter \(d_T\), relates to the lung volume as follows
\begin{equation}
  \frac{d_T}{d_A} = \left(\frac{V_L}{V_\text{FRC}}\right)^{1/3}.
\end{equation}
These equations specify \(A_T\) for all \(x\) and \(t\), which we will use to calculate the velocity profile.

We can derive the continuity equation for our problem by considering how the total volume of air varies over a section in the RT.
If the section in question starts at a point \(x=a\) and ends at a point \(x=b\), then the total volume of air varies with the flux over the section boundaries
\begin{equation}
  \label{eq:flux}
  \int_a^b \frac{\partial }{\partial t} A_T \,dx = F(a) - F(b),
\end{equation}
where \(F(x)\) is the flux function.
Since advection only happens in the conducting part of the airway \(A_A\) (not \(A_T\)), the flux function is \(F(x, t)=u(x, t)A_A(x)\).
Inserting this into~\eqref{eq:flux} and simplifying yields,
\begin{equation}
  \int_a^b \frac{\partial }{\partial t} A_T \,dx = - u A_A \Big|_a^b.
\end{equation}
Solving this for \(u\), we obtain
\begin{equation}
  \label{eq:velocityprofile}
  u(b, t) = \frac{1}{A_A(b)}\left[u(a, t) A_A(a) - \int_a^b \frac{\partial }{\partial t} A_T(x, t) \,dx\right],
\end{equation}
which holds for all \(a\), \(b\), and \(t\).
This last property allows us to calculate \(u(x, t)\): we can simply fill in whatever \(b\), because we already know \(A_T(x, t)\) and \(u(0, t)\).
Indeed, the inlet velocity is simply \(u(0, t)=\frac{\partial}{\partial t} V_L(t)/A_A(0)\).
Note that in the entire derivation here, we have assumed a constant air density \(\rho\).

Luckily for us, this calculation can be decoupled from the partial differential equation for the aerosol concentrations, because the ``continuous'' quantity in the continuity equation is the volume of air, which we assume to be incompressible, and that it does not depend on the concentration of aerosols.
In this way, the velocity profile only depends on the breathing pattern specified, and can therefore be calculated ahead of time.
In fact, it can be calculated analytically for most breathing functions.
The numerical approximation will be treated in Section~\ref{subsec:numericalvelocity}.

\section{Calculating the deposition fraction}
The deposition fraction is the ratio of the total number of particles introduced into the respiratory tract, and the amount of deposited particles.
The former is easy to calculate: it is the influx of particles into the respiratory tract (RT), and since this only happens at the inlet (trachea), the expression is as follows
\begin{equation}
  \text{total RT} = \int_0^{T/2} q(0,t) A_A(0) u(0, t)\,dt,
\end{equation}
where \(T\) is the period of the breathing cycle.
Note that we only integrate over half of a period: the inspiration phase.

The total deposition is simply the deposition term of~\eqref{eq:GDE} integrated over \(G\), which could be a generation or the entire respiratory tract, depending on what we're interested in.
In this case, we do integrate over the entire period, because we want the deposition fraction over a whole breathing cycle.
The total deposition is shown below:
\begin{equation}
  \text{total deposition} = \int_0^T\int_G q(x,t) V_d(x,t) \Gamma(x,t)\,dx\,dt.
\end{equation}
The deposition fraction is the ratio of the two previous expressions, as follows~\cite{eulerian}
\begin{equation}
  \text{deposition fraction} = \frac{\int_0^T\int_G q(x,t) V_d(x,t) \Gamma(x,t)\,dx\,dt}{\int_0^{T/2} q(0,t) A_A(0) u(0, t)\,dt}.
\end{equation}
% The denominator denotes the total amount of particles that enter the respiratory tract, and therefore we only integrate it over half a period (inspiration), and only at the inlet, since (by the boundary conditions) the flux is zero at \(x=L\).
% (And the integrand is the flux of particles.)

Several numerical integration methods are possible, in this case we use a standard library that implements the trapezoidal rule.
The reason we choose this integration method instead of e.g. Simpson's rule, is because we require the integration to be independent of our starting and ending points.
This will play a role when we are calculating local deposition (as opposed to total deposition).
If we use the trapezoidal rule, then we ensure that local and total deposition calculations do not differ.

\section{Aerosol parameters and other constants}
We are primarily concerned with comparing the deposition rates of aerosols of different sizes.
However, the particle diameter has an effect on a number of other aerosol properties.
In this section, these properties are discussed as well as how they depend on the aerosol diameter.

First, the particle density ranges from about \SIrange{0.5}{3.0}{\gram\per\centi\meter\cubed}~\cite{amt-9-859-2016}.
We will take a standard value of~\SI{1.0}{\gram\per\centi\meter\cubed}
Note that this is the density of a single particle, and so it does not depend on the concentration of the particle in the fluid.

Next, we have the Cunningham slip correction factor.
This correction reduces the amount of drag force on particles (because of slip effects).
It depends on the particle diameter \(d\) and the mean free path \(\lambda\):
\begin{equation}
  C = 1 + \frac{\lambda}{d} \left[2.34 + 1.05\exp{\left(-0.39 \frac{d}{\lambda}\right)}\right].
\end{equation}
The Cunningham correction factor for a range of particle sizes is shown in Table~\ref{tab:cunningham}.

Another factor that depends heavily on the particle diameter is the particle relaxation time.
This characterises the time that a particle needs to adjust its velocity when exposed to a new environment.
The shorter the time, the quicker the particle adjusts to new force conditions.
Because of inertial effects, smaller particles tend to have a much shorter relaxation time than large particles, which tend to maintain their old path.
The expression for the relaxation time is as follows
\begin{equation}
  \tau = \frac{\rho d^2 C}{18 \mu}.
\end{equation}
The relaxation time is used for calculating Stokes' number, which we need to calculate the deposition velocity due to inertial effects.

In addition to particle-specific properties, we also need to take into account properties of the air in the lungs.
The density of air is \SI{1}{\kilogram\per\metre\cubed}, and its viscosity is \SI{18.1e-6}{\pascal\second}.
The mean free path of air (used in the previous paragraphs) is \SI{0.066e-6}{\metre}~\cite{JENNINGS1988159}.
Finally, we assume the temperature to be room temperature (\SI{293}{\kelvin}), and we discount warming up of the air in our calculations.

\begin{table}[h]
  \centering
  \caption{Cunningham slip correction factor and the particle relaxation time for a range of particle sizes.}
  \label{tab:cunningham}
  \begin{tabular}{SS[round-precision=3,round-mode=figures]S[round-precision=3,round-mode=figures]}
    \toprule
    {Particle diameter [\si{\micro\metre}]} & {Cunningham slip correction} & {Relaxation time [\si{\second}]} \\ \midrule
    0.01 & 22.976364031989004 & 2.1156872957632604E-08 \\
    0.1  & 2.928199740822716  & 2.6963165200945815E-07 \\
    1    & 1.1546281254967605 & 1.0631934857244572E-05 \\
    10   & 1.015444           & 9.350313075506448E-04  \\
    \bottomrule
  \end{tabular}
\end{table}

\section{Breathing patterns}
\label{sec:breathingpatterns}
Respiration generally refers to the absorption of \ce{O2} and the removal of \ce{CO2} from the body.
Breathing functions to move air into and out of the lungs through the trachea, where it undergoes a gas exchange in the alveoli.
During a normal respiratory pattern, the most important factor describing breathing mechanics is the tidal volume \(V_T\).
This is the amount of air inhaled or exhaled in one breath.
After each breath, the lungs still contain a residual volume that cannot be voluntarily expirated, as well as some expiratory reserve volume that is the amount of tidal expiration that can be exhaled with maximum effort.
Similarly, during normal breathing, there is some inspiratory reserve volume that can be inhaled with maximum effort.
For an illustration of the different respiratory volumes refer to Figure~\ref{fig:lungvolumes}.
\begin{figure}[h]
  \includegraphics[scale=1]{"../Images/ganong-picture.png"}
  \caption{Respiratory volumes and capacities for an average young adult male~\cite{breathingrate}.}
  \label{fig:lungvolumes}
\end{figure}

For an average young adult male, the total lung capacity is \SI{5900}{\milli\litre}, and the tidal volume during relaxed, quiet breathing is \SI{500}{\milli\litre}~\cite{breathingrate}.
The medical term for relaxed breathing is eupnoea.
In this thesis, we will investigate breathing patterns during exercise, also known as hyperpnea.
Breathing patterns like Biot's respiration and Cheyne-Stokes respiration that occur during drug overdoses or in a clinical setting will not be investigated.
Likewise, hyperventilation and hypoventilation will also not be considered, because although these situations may have a significant effect on the deposition fraction, they are also infrequent and of short duration.

Hyperpnea is the body's response to requiring more oxygen.
It primarily results in taking deeper breaths, or an increase in the tidal volume, but it can also result in faster breathing.
Usually, hyperpnea is a response to the body's activity or environment; for example, exercise or high altitude.
We will both investigate deeper breathing as well as changing the respiratory rate.
