\chapter{Introduction}
\label{chap:introduction}
Aerosols are by no means a modern invention.
They have been around since before humans inhabited the earth, and indeed, the English language has a lot of words to describe aerosol groups: dust, mist, fumes, smoke, etc.
Aerosols are small particles suspended in air or other gases, so the term not only encompasses the environmental phenomena described above, but also deodorants or cough droplets.
People have studied aerosols for a long time, but recently, they have gained the eye of the public when discussing the environmental effects of \textit{soot} in large cities, or when discussing the infectiousness of COVID-19 through aerosols.

The focus of this study is to create a computational model of aerosol deposition in the lungs.
Knowledge of aerosol deposition and distribution in the lungs is of importance when studying environmental effects, or in clinical settings.
There is a lot more flexibility in computational models as opposed to empirical studies of aerosol deposition - it is possible to tweak every parameter to perform parametric studies on this deposition fraction.
The drawback is of course, that numerical models need extensive empirical verification before the results are deemed trustworthy enough.
But where the application of empirical models is restricted to the particular experimental conditions for which the model was tested, numerical models can vary the lung conditions, physiology of the patient, and even the morphology of the patient's lungs.
It is suddenly trivial to model any type of patient, where in a lab setting this could be more difficult to control.

The flexibility of computational models extends to more than just parametric variation.
Many considerations must be made in their creation, such as choosing whether to model 1 or 3 dimensions; choosing a stochastic or deterministic approach; which physical effects to take into account; which numerical method to use; up to even choosing to model the particles using Eulerian or Lagrangian mechanics.
The present study attempts to recreate a one dimensional model developed by~\textcite{eulerian}.
Current 3-dimensional models have the advantage of providing a complete description of aerosol dynamics, but have the disadvantage of requiring a lot of computational resources.
The 1-dimensional approach allows us to quickly determine the deposition ratios for particles of different sizes under a range of respiratory conditions.

We will use this model to examine the effect of various parameters on the deposition fraction.
Most notably, we will vary the breathing conditions like the tidal volume and the breathing rate, to see the effect on the total absorption.
This will be used to shine light on the infectiousness of COVID-19 through aerosols and the health hazards of smoke inhalation during the bushfires that devastated Australia in the summer of 2019-20.

Chapter~\ref{chap:theory} introduces the mechanics behind aerosol dynamics, and a description of lung morphology and lung models.
The numerical method used to model the aerosol dynamics is designed in Chapter~\ref{chap:numerics}, along with an analysis of its convergence.
After the relevant theory and numerics are introduced, validation of the model will be treated in Chapter~\ref{chap:modelverification}.
Extensive verification is needed to ensure that the model accurately describes aerosol deposition under a range of conditions, which will be the subject of Chapter~\ref{chap:results}.
Here, we perform parametric studies to analyse the effect of different breathing conditions on the deposition fraction.
Finally, Chapter~\ref{chap:conclusion} summarises the findings and provides some recommendations for further research.
