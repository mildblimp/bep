"""
This module computes the exact solution in the case of 1D advection-diffusion,
with a Dirichlet condition on the left, and Neumann on the right.
Solution taken from https://naldc.nal.usda.gov/download/CAT82780278/PDF
"""


import numpy as np
import solution as da
from scipy.optimize import fsolve


class ExactAirway(da.Solution):
    """
    Represent a 1d pipe in a advection/diffusion situation
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.R = 1  # hardcoded value
        self.D = self.D_interface[0]
        self.initial_condition = np.append(self.left_boundary[1],
                                           self.initial_condition)
        self.check_analytical_conditions()
        self.velocity = self.velocity[0]
        self.sol = self.make_df(self.solve())

    def check_analytical_conditions(self):
        if not all([self.left_boundary[0] == "dirichlet",
                    self.right_boundary[0] == "neumann"]):
            raise ValueError("boundaries must be of type Dirichlet and Neumann"
                             " respectively")
        if not max(self.D_interface) == min(self.D_interface):
            raise ValueError("analytical solution does not support nonconstant"
                             " diffusion coefficient")
        if not max(self.velocity) == min(self.velocity):
            raise ValueError("analytical solution does not support nonconstant"
                             " velocity")
        if "deposition" in self.run_flags:
            raise ValueError("analytical solution does not support deposition")
        return True

    def A3_get_eigenvalues(self):
        """
        Get eigenvalues for the 1D advection-diffusion problem.

        Returns
        -------
        numpy array
            Array of eigenvalues
        """
        initial_guess = np.arange(2.5, 2.5+3.1*self.N, 3.1)
        # These hard-coded values come from a graphical analysis of the function
        # below.
        f = lambda m: m/np.tan(m) + self.velocity*self.L/(2*self.D)
        return fsolve(f, initial_guess)

    def A3_exact(self, t):
        """
        Get solution of the 1D advection-diffusion equation for given t

        Parameters
        ----------
        t: float
            t coordinate

        Returns
        -------
        numpy array
            concentration at the x array, at a time t
        """
        x, R, D, v, L, c_i, c_0, N = [self.x, self.R, self.D, self.velocity,
                                      self.L, self.initial_condition,
                                      self.left_boundary[1], self.N]
        # m is an eigenvalue
        summand = lambda m: (
            (2*m * np.sin(m*x/L)
             * np.exp(v*x/(2*D) - v**2*t/(4*D*R) - m**2*D*t/(L**2*R)))
            / (m**2 + (v*L/(2*D))**2 + v*L/(2*D))
        )
        eigenvalues = self.A3_get_eigenvalues()
        sum_array = np.array([summand(l) for l in eigenvalues])
        u = c_i + (c_0 - c_i)*(1-np.sum(sum_array, axis=0))
        return u

    def solve(self):
        mat = np.array([self.A3_exact(t) for t in self.T[1:]])
        mat = np.vstack([self.initial_condition, mat])
        return mat


def exact_steady_state(x, D, v, L, c_0, c_L):
    """
    Get steady state solution of 1D convective-diffusion equation for given `x`

    Parameters
    ----------
    x: numpy array
        x coodinates
    D: float
        Coefficient of the diffusive term
    v: float
        Velocity of the convective flow
    L: float
        Length of the tract
    c_0: float
        Dirichlet boundary condition on the left side
    c_L: float
        Dirichlet boundary condition on the right side

    Returns
    -------
    numpy array
        Concentrations at positions `x`
    """
    P = v*L/D  # Peclet number
    return c_0 + (c_L-c_0)*(np.exp(P*x/L)-1)/(np.exp(P)-1)


if __name__ == "__main__":
    analytical_solution = ExactAirway()
    analytical_solution.surface_plot()
    analytical_solution.time_evolution_plot(6)
