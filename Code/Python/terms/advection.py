"""
This module contains the discretisation matrix for advection of the 1d general
dynamic aerosol equation.
"""


import numpy as np


class Advection:
    """Represents a advection discretisation matrix

    Parameters
    ----------
    x: numpy array
        Grid points
    velocity: numpy array
        fluid velocity
    density: float
        fluid density
    A_A: numpy array
        Cross sectional area of all airways
    left_boundary: tuple
        Boundary condition on the left side
    right_boundary: tuple
        Boundary condition on the right side

    Returns
    -------
    numpy array
        Advection matrix, A, in diagonal ordered form
    numpy array
        Advection vector, b, for 1D advection, using the upwind scheme
    """

    def __init__(self, x, velocity, density, A_A,
                 left_boundary, right_boundary):
        self.x = x
        self.velocity = velocity
        self.density = density
        self.A_A = A_A
        self.left_boundary = left_boundary
        self.right_boundary = right_boundary
        self.A, self.b = self.sparse_advection_matrix()

    def __repr__(self):
        data = {
            'x': self.x,
            'velocity': self.velocity,
            'density': self.density,
            'A_A': self.A_A,
            'left_boundary': self.left_boundary,
            'right_boundary': self.right_boundary,
        }
        return ("Diffusion({x}, {velocity}, {density}, {A_A}, {left_boundary}, "
                "{right_boundary})").format(**data)

    def __str__(self):
        return (self.A, self.b)

    def sparse_advection_matrix(self):
        N = len(self.x)
        A = np.zeros([3, N])
        b = np.zeros(N)
        # interior points
        coefficients = self.A_A * self.velocity * self.density
        # Take only the first element of velocity. We assume that velocity has
        # the same sign everywhere.
        if self.velocity[0] == 0:
            return (A, b)
        elif self.velocity[0] > 0:
            # west
            A[0, :-1] = coefficients
            # interior
            A[1, :-1] = -coefficients
            # The boundary point has no coefficient in the upstream model, so we
            # just copy the one before that.
            A[1, -1] = -coefficients[-1]
        elif self.velocity[0] < 0:
            coefficients *= -1
            # east
            A[2, 1:] = coefficients
            # interior
            A[1, 1:] = -coefficients
            # The boundary point has no coefficient in the upstream model, so we
            # just copy the one before that.
            A[1, 0] = -coefficients[0]
        return (A, b)
