"""
This module contains methods for the deposition of particles in the aerosol
dynamic equation.
"""


import numpy as np
from constants import gravity, boltzmann_constant, EPS
import warnings


class Deposition:
    """Represents a Deposition discretisation matrix

    Parameters
    ----------
    x: numpy array
        Grid points
    airway_diameter: numpy array
        Diameter of RT at every x
    number_airways_per_generation: numpy array
        Number of airways per generation
    fluid_viscosity: float
        Viscosity of air
    velocity: numpy array
        Velocity of at every x
    fluid_density: float
        Density of air
    branching_angle: numpy array
        Branching angles per generation
    generation_length: numpy array
        Length of the generations
    cumulative_length: numpy array
        Cumulative generation lengths
    grid_spacing: numpy array
        Grid spacing
    gravitational_settling_velocity: numpy array
        Gravitational settling velocity for all x
    particle_relaxation_time: float
        Relaxation time of the particle
    effective_diffusion_coefficient: numpy array
        Diffusion coefficients for all x
    brownian_diffusion_coefficient: float
        Brownian diffusion coefficient for all x

    Returns
    -------
    numpy array
        Deposition matrix, A, in diagonal ordered form
    numpy array
        Deposition vector, b
    """

    def __init__(self, x, airway_diameter, number_airways_per_generation,
                 fluid_viscosity, velocity, fluid_density, branching_angle,
                 generation_length, cumulative_length, grid_spacing,
                 gravitational_settling_velocity, particle_relaxation_time,
                 effective_diffusion_coefficient,
                 brownian_diffusion_coefficient):
        self.x = x
        self.airway_diameter = airway_diameter
        self.number_airways_per_generation = number_airways_per_generation
        self.fluid_viscosity = fluid_viscosity
        self.velocity = velocity
        self.fluid_density = fluid_density
        self.branching_angle = branching_angle
        self.generation_length = generation_length
        self.cumulative_length = cumulative_length
        self.grid_spacing = grid_spacing
        self.gravitational_settling_velocity = gravitational_settling_velocity
        self.particle_relaxation_time = particle_relaxation_time
        self.effective_diffusion_coefficient = effective_diffusion_coefficient
        self.brownian_diffusion_coefficient = brownian_diffusion_coefficient
        self.wetted_perimeter = self.get_wetted_perimeter()
        # Brownian diffusion velocity
        if np.any(abs(self.velocity) < EPS) == 0:
            # This if statement is to avoid inf values when dividing by
            # velocity. In that case we assume that the diffusion velocity is
            # 0.
            self.reynolds_number = self.get_reynolds_number()
            self.schmidt_number = self.get_schmidt_number()
            self.dimensionless_length = self.get_dimensionless_length()
            self.sherwood_number = self.get_sherwood_number()
            self.diffusion_velocity = self.get_brownian_diffusion_velocity()
        else:
            self.diffusion_velocity = 0
        # Impaction velocity
        self.stokes_number = self.get_stokes_number()
        self.impact_velocity = self.get_impact_velocity()
        self.deposition_velocity = sum([
            self.gravitational_settling_velocity,
            self.diffusion_velocity,
            self.impact_velocity
        ])
        self.A, self.b = self.sparse_deposition_matrix()

    def __repr__(self):
        data = {
            'x': self.x,
            'airway_diameter': self.airway_diameter,
            'number_airways_per_generation': self.number_airways_per_generation,
            'fluid_viscosity': self.fluid_viscosity,
            'velocity': self.velocity,
            'fluid_density': self.fluid_density,
            'branching_angle': self.branching_angle,
            'generation_length': self.generation_length,
            'cumulative_length': self.cumulative_length,
            'grid_spacing': self.grid_spacing,
            'gravitational_settling_velocity': self.gravitational_settling_velocity,
            'particle_relaxation_time': self.particle_relaxation_time,
            'effective_diffusion_coefficient': self.effective_diffusion_coefficient,
            'brownian_diffusion_coefficient': self.brownian_diffusion_coefficient,
        }
        return ("Deposition({x}, {airway_diameter},"
                " {number_airways_per_generation}, {fluid_viscosity},"
                " {velocity}, {fluid_density}, {branching_angle},"
                " {generation_length}, {cumulative_length}, {grid_spacing},"
                " {gravitational_settling_velocity},"
                " {particle_relaxation_time},"
                " {effective_diffusion_coefficient},"
                " {brownian_diffusion_coefficient})").format(**data)

    def __str__(self):
        return (self.A, self.b)

    def get_wetted_perimeter(self):
        """
        Calculate the wetted wetted perimeter.

        Returns
        -------
        numpy array
            Wetted perimeter
        """
        return (self.number_airways_per_generation *
                np.pi * self.airway_diameter)

    def get_brownian_diffusion_velocity(self):
        """
        Calculate the Brownian diffusion velocity.

        Returns
        -------
        numpy array
            Brownian diffusion velocity
        """
        return self.brownian_diffusion_coefficient * \
            self.sherwood_number / self.airway_diameter

    def get_dimensionless_length(self):
        """
        Calculate the dimensionless length.

        Returns
        -------
        numpy array
            Dimensionless length
        """
        # This is different than what the paper says. The paper takes the total
        # x, that is, from the beginning of the RT.
        absolute_distance = self.x - self.cumulative_length
        return (absolute_distance / (self.airway_diameter *
                                     self.reynolds_number * self.schmidt_number))

    def get_sherwood_number(self):
        """
        Calculate Sherwood's number.

        Returns
        -------
        numpy array
            Sherwood number
        """
        sherwood_number = np.zeros(len(self.x))
        threshold = 0.01
        smaller_than_indices = np.where(self.dimensionless_length <= threshold)
        larger_than_indices = np.where(self.dimensionless_length > threshold)
        with warnings.catch_warnings():
            # We suppress the warning, because we later replace the inf values
            # before returning sherwood_number.
            warnings.filterwarnings(
                "ignore", message="divide by zero encountered in power")
            sherwood_number[smaller_than_indices] = (
                1.077 * np.power(
                    self.dimensionless_length[smaller_than_indices], -1/3) - 0.7)
        sherwood_number[larger_than_indices] = (
            3.657 + 6.874
            * np.power(1000 * self.dimensionless_length[larger_than_indices], -0.488)
            * np.exp(-57.2 * self.dimensionless_length[larger_than_indices])
        )
        # remove inf values (hacky)
        sherwood_number[sherwood_number > 1E308] = 3.657
        return sherwood_number

    def get_reynolds_number(self):
        """
        Calculate Reynolds number.

        Returns
        -------
        numpy array
            Reynolds number
        """
        return abs(self.fluid_density * self.velocity * self.airway_diameter
                   / self.fluid_viscosity)

    def get_schmidt_number(self):
        """
        Calculate particle Schmidt number.

        Returns
        -------
        float
            Schmidt number
        """
        return self.fluid_viscosity / \
            (self.fluid_density * self.brownian_diffusion_coefficient)

    def get_stokes_number(self):
        """
        Calculate Stokes' number.

        Returns
        -------
        numpy array
            Stokes' number
        """
        return abs(self.particle_relaxation_time * self.velocity
                   / self.airway_diameter)

    def get_impact_velocity(self):
        """
        Calculate the impact velocity.

        Returns
        -------
        numpy array
            impact velocity
        """
        relative_distance = ((self.x - self.cumulative_length)
                             / self.generation_length)
        truth_array = relative_distance >= 0.8
        impact_velocity = (truth_array * (
            self.particle_relaxation_time * self.velocity**2 * self.branching_angle
            / (0.2 * self.generation_length)
        ))
        return impact_velocity

    def sparse_deposition_matrix(self):
        N = len(self.x)
        A = np.zeros([3, N])
        b = np.zeros(N)
        # interior points
        dxs = 0.5*(self.grid_spacing[1:] + self.grid_spacing[:-1])
        dxs = np.concatenate(([0.5*dxs[0]], dxs, [0.5*dxs[-1]]))
        coefficients = -(self.deposition_velocity *
                         self.wetted_perimeter * dxs)
        A[1, :] = coefficients
        # vector b is empty because we do not have to linearise the source term
        return (A, b)
