"""
This module contains the discretisation matrix for diffusion of the 1d general
dynamic aerosol equation.
"""


import numpy as np


class Diffusion:
    """Represents a diffusion discretisation matrix

    Parameters
    ----------
    x: numpy array
        Grid points
    D_interface: numpy array
        Diffusion constants for faces between grid points
    A_T: numpy array
        Cross sectional area of all airways
    left_boundary: tuple
        Boundary condition on the left side
    right_boundary: tuple
        Boundary condition on the right side
    grid_spacing: numpy array
        Mesh grid distance

    Returns
    -------
    numpy array
        Diffusion matrix, A, in diagonal ordered form
    numpy array
        Diffusion vector, b, for 1D diffusion
    """

    def __init__(self, x, D_interface, A_T, left_boundary, right_boundary,
                 grid_spacing):
        self.x = x
        self.D_interface = D_interface
        self.A_T = A_T
        self.left_boundary = left_boundary
        self.right_boundary = right_boundary
        self.grid_spacing = grid_spacing
        self.A, self.b = self.sparse_diffusion_matrix()

    def __repr__(self):
        data = {
            'x': self.x,
            'D_interface': self.D_interface,
            'A_T': self.A_T,
            'left_boundary': self.left_boundary,
            'right_boundary': self.right_boundary,
            'grid_spacing': self.grid_spacing,
        }
        return ("Diffusion({x}, {D_interface}, {A_T}, {left_boundary}, "
                "{right_boundary}, {grid_spacing})").format(**data)

    def __str__(self):
        return (self.A, self.b)

    def sparse_diffusion_matrix(self):
        N = len(self.x)
        A = np.zeros([3, N])
        b = np.zeros(N)
        # interior points
        coefficients = self.A_T * self.D_interface / self.grid_spacing
        # west
        A[0, :-1] = coefficients
        # east
        A[2, 1:] = coefficients
        # interior
        A[1, :] = -(A[0, :] + A[2, :])
        return (A, b)
