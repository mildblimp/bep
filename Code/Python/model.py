"""
This module can be used to simulate the inhalation of particles in the lungs,
using a 1-D advection-diffusion model.
"""


import numpy as np
from scipy.sparse import dia_matrix, identity, SparseEfficiencyWarning
from scipy.sparse.linalg import spsolve
from scipy import integrate
import solution as da
from terms.advection import Advection
from terms.diffusion import Diffusion
from terms.deposition import Deposition

import warnings
warnings.simplefilter('ignore', SparseEfficiencyWarning)


class Airway(da.Solution):
    """
    Represents an airway in the lungs using a Weibel-like geometry.

    Takes the output of `Constructor.unpack()` as arguments.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.total_deposition = []
        self.A_A_interface = self.grid_to_interface(self.A_A)
        self.initialise_time_dependent_variables()
        self.sol = self.make_df(self.theta_method())

    def initialise_time_dependent_variables(self):
        self.velocity = self.velocity_all[0, :]
        self.v_interface = self.grid_to_interface(self.velocity)
        self.airway_diameter = self.airway_diameter_all[0, :]
        self.left_boundary = self.left_boundary_all[0]
        self.A_T = self.A_T_all[0, :]
        self.A_T_interface = self.grid_to_interface(self.A_T_all[0, :])
        self.diffusion_constant = self.get_effective_diffusion_coefficient()
        self.D_interface = self.grid_to_interface(self.diffusion_constant)

    def grid_to_interface(self, grid_values):
        """
        Take grid points values and turn them into values specified on interfaces.

        Parameters
        ----------
        grid_values: numpy array
            Grid values

        Returns
        -------
        numpy array
            Values specified on interface
        """
        # This function used to calculate the harmonic mean. This was changed
        # to ensure a mass balance when stepping over generation boundaries.
        return grid_values[:-1]

    def build_discretisation_matrix(self):
        """
        Build the discretisation matrix

        Returns
        -------
        numpy array
            Discretisation matrix A
        numpy array
            Discretisation vector b
        """
        N = len(self.x)
        A = np.zeros([3, N])
        b = np.zeros(N)
        # OPTIMIZE: This can probably be optimized if every separate term does
        # not make its own matrix, and instead adds to a given one.
        if "diffusion" in self.run_flags:
            diffusion = Diffusion(
                self.x, self.D_interface, self.A_T_interface,
                self.left_boundary, self.right_boundary, self.grid_spacing
            )
            A += diffusion.A
            b += diffusion.b
        if "advection" in self.run_flags:
            advection = Advection(
                self.x, self.v_interface, self.fluid_density, self.A_A_interface,
                self.left_boundary, self.right_boundary
            )
            A += advection.A
            b += advection.b
        if "deposition" in self.run_flags:
            deposition = Deposition(
                self.x, self.airway_diameter,
                self.number_airways_per_generation, self.fluid_viscosity,
                self.velocity, self.fluid_density, self.branching_angle,
                self.generation_length, self.cumulative_length,
                self.grid_spacing, self.gravitational_settling_velocity,
                self.particle_relaxation_time,
                self.diffusion_constant, self.brownian_diffusion_coefficient
            )
            A += deposition.A
            b += deposition.b
            # For deposition calculations:
            self.deposition_coefficients = deposition.A[1, :]
        A, b = self.add_boundary_conditions(A, b)
        offsets = [-1, 0, 1]
        A = dia_matrix((A, offsets), shape=(A.shape[1], A.shape[1]))
        return (A, b)

    def add_boundary_conditions(self, A, b):
        """
        Add boundary conditions to A and b.

        When the boundary condition is of type Neumann, advection is disregarded
        depending on whether the velocity is positive or negative (because we
        are using the upwind scheme).

        For a boundary condition of type Dirichlet, the matrix is not resized.
        Instead, the effect of the boundary is placed in vector b, and the effect
        of the boundary in the matrix A is set to zero. The Dirichlet conditions
        are added later.

        Parameters
        ----------
        A: numpy array
            Discretisation matrix A
        b: numpy array
            Discretisation vector b

        Returns
        -------
        numpy array
            Discretisation matrix A with boundary conditions
        numpy array
            Discretisation vector b with boundary conditions

        See Also
        --------
        fix_boundaries: Add Dirichlet conditions.
        """
        left = 0
        right = 0
        if self.left_boundary[0] == "dirichlet":
            if "diffusion" in self.run_flags:
                left += self.A_T_interface[0] * \
                    self.D_interface[0] / self.grid_spacing[0]
            if "advection" in self.run_flags and self.velocity[0] > 0:
                left += self.A_A_interface[0] * \
                    self.velocity[0] * self.fluid_density
            b[1] = left * self.left_boundary[1]
            A[0, 0] = 0
        elif self.left_boundary[0] == "neumann" and "diffusion" in self.run_flags:
            b[0] = self.left_boundary[1]
        if self.right_boundary[0] == "dirichlet":
            if "diffusion" in self.run_flags:
                right += self.A_T_interface[-1] * \
                    self.D_interface[-1] / self.grid_spacing[-1]
            if "advection" in self.run_flags and self.velocity[0] < 0:
                right -= self.A_A_interface[-1] * \
                    self.velocity[-1] * self.fluid_density
            b[-2] = right * self.right_boundary[1]
            A[2, -1] = 0
        elif self.right_boundary[0] == "neumann" and "diffusion" in self.run_flags:
            b[-1] = self.right_boundary[1]
        return (A, b)

    def handle_grid_spacing(self):
        """
        Return grid spacing with a mesh based centre.

        This grid spacing, unlike self.grid_spacing is mesh based. That is, the
        spacing has a meshpoint in the center instead of an edge.

        Returns
        -------
        numpy array
            mesh based grid spacing
        """
        dxs = 0.5*(self.grid_spacing[1:] + self.grid_spacing[:-1])
        dxs = np.concatenate(([0.5*dxs[0]], dxs, [0.5*dxs[-1]]))
        return dxs

    def get_total_deposition(self):
        """
        Calculate the local (per generation) deposition per time step.

        Returns
        -------
        list of numpy arrays
            local deposition per time step
        """
        w = np.copy(self.w)
        dxs = self.handle_grid_spacing()

        bin = np.digitize(self.x, self.cumulative_length)-1
        change_indices = np.concatenate(
            ([0],
             np.where(bin[:-1] != bin[1:])[0]+1,
             [-2])
        )
        change_pairs = zip(change_indices[:-1], change_indices[1:]+1)
        local_deposition = []
        for pair in change_pairs:
            local_deposition.append(
                integrate.trapz(
                    (-self.deposition_coefficients[slice(*pair)]
                     * w[slice(*pair)] / dxs[slice(*pair)]),
                    self.x[slice(*pair)])
            )
        self.total_deposition.append(local_deposition)

    def get_effective_diffusion_coefficient(self):
        """
        Calculate effective diffusion coefficient.

        Returns
        -------
        numpy array
            Effective diffusion coefficient
        """
        equals_array = (self.velocity == 0)
        greater_array = (self.velocity > 0)
        smaller_array = (self.velocity < 0)
        return abs(equals_array * self.brownian_diffusion_coefficient
                   + greater_array * (self.brownian_diffusion_coefficient
                                      + (1.08 * self.velocity * self.airway_diameter))
                   + smaller_array * (self.brownian_diffusion_coefficient
                                      + (0.37 * self.velocity * self.airway_diameter))
                   )

    def set_time_dependent_terms(self, time_index):
        self.A_T = self.A_T_all[time_index, :]
        self.A_T_interface = self.grid_to_interface(
            self.A_T_all[time_index, :])
        self.airway_diameter = self.airway_diameter_all[time_index, :]
        self.velocity = self.velocity_all[time_index, :]
        self.v_interface = self.grid_to_interface(self.velocity)
        self.diffusion_constant = self.get_effective_diffusion_coefficient()
        self.D_interface = self.grid_to_interface(self.diffusion_constant)
        self.left_boundary = self.left_boundary_all[time_index]

    def fix_boundaries(self):
        """
        Add Dirichlet conditions to `self.w`
        """
        if self.left_boundary[0] == "dirichlet":
            self.w[0] = self.left_boundary[1]
        if self.right_boundary[0] == "dirichlet":
            self.w[-1] = self.right_boundary[1]

    def theta_method(self):
        """
        Solve the boundary value problem with the theta method.

        For details and readability, consult the thesis.

        Returns
        -------
        numpy array
            Concentration of aerosol at every x and t
        """
        self.w = self.initial_condition
        self.N_theta = len(self.w)
        self.I_theta = identity(self.N_theta, format='dia')
        w_array = [self.w]
        self.spacing_reciprocal = 1/self.handle_grid_spacing()
        A, b = self.build_discretisation_matrix()
        self.fix_boundaries()
        if "deposition" in self.run_flags:
            self.get_total_deposition()
        for idx_t in range(1, len(self.T)-1):
            w_old = np.copy(self.w)
            A_old, b_old = A.copy(), np.copy(b)
            A_T_old = np.copy(self.A_T)
            self.set_time_dependent_terms(idx_t)
            A, b = self.build_discretisation_matrix()
            factor = dia_matrix((self.A_T, 0), shape=(len(self.A_T), len(self.A_T))) \
                - (self.dt * self.theta
                   * A.multiply(self.spacing_reciprocal[:, np.newaxis]))
            inv_factor = spsolve(factor, self.I_theta)
            self.w = inv_factor.dot(np.ravel(
                (dia_matrix((A_T_old, 0), shape=(len(A_T_old), len(A_T_old)))
                 + self.dt * (1-self.theta)
                 * A_old.multiply(self.spacing_reciprocal[:, np.newaxis]))
                .dot(w_old)
                + (self.dt * self.spacing_reciprocal
                   * (self.theta * b + (1-self.theta) * b_old))))
            self.fix_boundaries()
            w_array.append(self.w)
            if "deposition" in self.run_flags:
                self.get_total_deposition()
        w_array = np.vstack(w_array)
        return w_array


if __name__ == "__main__":
    from constants import Constructor
    numerical_solution = Airway(**Constructor().unpack())
    numerical_solution.surface_plot()
    print(numerical_solution.deposition_fraction())
