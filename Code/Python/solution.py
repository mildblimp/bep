"""
This module is used to visualise the results of the solution of the 1D
advection-diffusion equation
"""


import numpy as np
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
import pandas as pd
from scipy import integrate
from constants import cumulative_length


class Solution():
    """A solution of a 1d advection-diffusion equation.

    The solution is stored in a pandas DataFrame and has various methods to
    display it.

    Parameters
    ----------
    boundaries: dict
    space_discretisation: dict
    time_integration: dict
    general: dict
    meta: dict
    deposition: dict

    Returns
    -------
    Solution of 1d advection diffusion equation in human lungs

    """

    def __init__(
        self,
        boundaries=None,
        space_discretisation=None,
        time_integration=None,
        general=None,
        meta=None,
        deposition=None
    ):
        for dict in [boundaries, space_discretisation, time_integration,
                     general, meta]:
            for key, value in dict.items():
                setattr(self, key, value)
        if deposition is not None:
            for key, value in deposition.items():
                setattr(self, key, value)
        self.sol = None

    def make_df(self, mat):
        """
        Create solution DataFrame

        Returns
        -------
        a pandas DataFrame
        """
        # We have to remove the last time point, because we cannot calculate
        # this using an implicit method, which is why the iteration stops one
        # time step early in theta_method in model.py
        self.T = self.T[:-1]
        return pd.DataFrame(mat, index=self.T, columns=self.x)

    def surface_plot(self):
        """
        Show a 3d surface plot
        """
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        X, T = np.meshgrid(self.x, self.T)
        surf = ax.plot_surface(X, T, self.sol.to_numpy(), cmap='viridis')
        fig.colorbar(surf)
        ax.set_xlabel('x')
        ax.set_ylabel('t')
        ax.set_zlabel('N')
        ax.set_zlim(-1, 2)
        plt.show()

    def time_evolution_plot(self, time_steps=0, times=[],
                            show_generations=False):
        """
        Show a time evolution plot of the concentration

        Parameters
        ----------
        time_steps: int
            Number of time steps to plot
        times: list
            Exact time steps to plot
        show_generations: bool
            Whether to show generation boundaries
        """
        if ((time_steps == 0 and len(times) == 0)
                or (time_steps != 0 and len(times) != 0)):
            raise ValueError(
                "should give exactly one of time_steps or times arguments")
        fig = plt.figure()
        ax = fig.gca()
        if len(times) == 0:
            times = [self.sol.index[i*self.sol.shape[0] //
                                    max(time_steps-1, 0)] for i in range(time_steps-1)]
            times.append(self.sol.index[-1])
        ilocs = [
            self.sol.index.get_loc(
                time,
                method='nearest') for time in times]
        ax.plot(self.sol.T.iloc[:, ilocs], color='k', linewidth=1)
        create_labels(times, self.sol, ax)
        ax.set_xlabel('x')
        ax.set_ylabel('concentration')
        ax.set_xlim([min(self.sol.columns), max(self.sol.columns)])
        if show_generations:
            show_plot_generations(ax)
        plt.show()

    def deposition_fraction(self, offset=0.0):
        """
        Get the deposition fractions for a breathing cycle.

        Parameters
        ----------
        offset: float
            Start calculation at this time step

        Returns
        -------
        dict
            Dictionary containing the local and total deposition fractions
        """
        period = 1/self.breathing_rate
        t_half = self.sol.loc[offset:offset+period/2].index
        N0 = self.sol.loc[offset:offset+period/2, 0]
        offset_idx = self.sol.index.get_loc(offset)
        v = self.velocity_all[offset_idx:offset_idx+len(N0), 0]
        total_RT = integrate.trapz(N0 * self.A_A[0] * v, t_half)

        t = self.sol.loc[offset:offset+period].index
        local_deposition = []
        deposition_array = np.array(self.total_deposition)
        for gen in range(24):
            local_deposition.append(
                integrate.trapz(
                    deposition_array[offset_idx:+offset_idx+len(t), gen], t)
            )
        local_deposition = np.array(local_deposition)
        total_deposition = sum(local_deposition)
        return {
            "total_deposition": total_deposition,
            "total_RT": total_RT,
            "deposition_fraction": total_deposition/total_RT,
            "local_deposition_fraction": local_deposition / total_RT,
        }


def create_labels(times, solution, axis, x_index=-1):
    """
    Create solution labels on a particular figure

    Parameters
    ----------
    times: list
        Times steps for the labels
    solution: Solution DataFrame
        DataFrame containing solution (`Solution.sol`)
    axis: matplotlib axis type
        Matplotlib axis to plot on
    x_index: int
        The X index for the labels

    Returns
    -------
    None
    """
    coords = solution.loc[times, solution.columns[x_index]]
    # to make the key a string
    coords = {
        "t={:.3}".format(key): value for key, value in zip(
            coords.index.map(str),
            coords.values)}
    for key, value in coords.items():
        axis.text(float(solution.columns[x_index]), value, key,
                  horizontalalignment='right', verticalalignment='top')


def show_plot_generations(axis):
    """
    Create generation indicators on a particular figure

    Parameters
    ----------
    axis: matplotlib axis type
        Matplotlib axis to plot on

    Returns
    -------
    None
    """
    y = axis.get_ylim()[1]
    # Alternate vertical alignment to save space in later generations
    verticalalignment = ['bottom', 'top']
    # BUG: cumulative_length is not dynamically obtained, which means that you
    # cannot show plot generations using Yeh and Schum geometry or when using
    # rescale_geometry=True (which is the default!)
    for idx, x in enumerate(cumulative_length):
        if idx < 8:
            va = verticalalignment[0]
        else:
            va = verticalalignment[idx % 2]
        axis.axvline(x=x, linestyle=':', color='k', linewidth=0.5)
        if ((idx > 11) and (idx % 3 != 0)) or (idx == 24):
            # We only plot some generation labels, to avoid clutter.
            continue
        axis.text(x=x, y=y, s="{}".format(idx), fontsize='small',
                  horizontalalignment='center', verticalalignment=va)
