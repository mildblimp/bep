"""
This module can be used to generate results and figures/images for use in the
thesis.
"""
import os

from constants import Constructor
from model import Airway
from solution import create_labels, show_plot_generations

from tikzplotlib import save as tikz_save
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker


# Directories
DIR = os.path.dirname(os.path.realpath(__file__))
BASE_DIR = os.path.abspath(os.path.join(DIR, '..', '..'))
TEX_IMAGE_DIR = os.path.abspath(os.path.join(BASE_DIR, 'Images', 'Graphs'))
OTHER_IMAGE_DIR = os.path.abspath(
    os.path.join(BASE_DIR, 'Images', 'Unused', 'parametrical_tests'))
LITERATURE_RESULTS_DIR = os.path.abspath(
    os.path.join(BASE_DIR, 'Datasets', 'Literature'))
RESULTS_DIR = os.path.abspath(os.path.join(BASE_DIR, 'Datasets', 'Results'))
# Plot constants to make every plot look consistent
COLOR = 'k'
LINEWIDTH = 1
LINESTYLES = [':', '-.', '--', '-']
TICK_LABEL_STYLE = """ticklabel style={
  /pgf/number format/fixed,
}"""


def _handle_output_file(output_filename, subfigure=False):
    """
    Plot or save a matplotlib figure in .png or .tex format

    Parameters
    ----------
    output_filename: str or None
        Either none for a direct plot, or an output filename to save. Two
        extension are supported: .tex and .png
    subfigure: bool
        Specify if the figure should be a subfigure (only for saving in .tex
        format).

    Returns
    -------
    None
    """
    if output_filename is None:
        plt.show()
    else:
        if (ext := os.path.splitext(output_filename)[1]) == '.tex':
            extra_axis_parameters = [
                'clip=false',
                'log ticks with fixed point',
                TICK_LABEL_STYLE,
                'scaled y ticks = false',
            ]
            if subfigure:
                extra_axis_parameters.append(r'width=\textwidth')
            output_filename = os.path.join(TEX_IMAGE_DIR, output_filename)
            tikz_save(
                output_filename,
                strict=True,
                extra_axis_parameters=extra_axis_parameters,
            )
        elif ext == '.png':
            output_filename = os.path.join(OTHER_IMAGE_DIR, output_filename)
            plt.savefig(output_filename)


def _handle_time_indices(time_indices):
    """
    Calculate time indices if not specified.

    Parameters
    ----------
    time_indices: None or list
        Indices of time steps
    Returns
    -------
    list
        list of time indices
    """
    if time_indices is None:
        max_index = len(T)
        time_indices = [0, max_index//3, 2*max_index//3, -1]
    return time_indices


def get_deposition_fractions(output_filename, start=-2, stop=1, number=10,
                             extra_constructor_properties={}):
    """Calculate the deposition fractions for a range of particle diameters.

    Particle diameters vary in logspace, and results are saved to a file.

    Parameters
    ----------
    output_filename: str
        output filename
    start: int
        start diameter (integer, where 0 is 1e-6, 1 is 1e-5 etc)
    stop: int
        stop diameter (like start)
    number: int
        number of diameters to calculate
    extra_constructor_properties: dict
        Extra parameters for `Constructor`
    """
    # Calculating deposition fractions
    diameters = np.logspace(start, stop, number) * 1e-6
    deposition_values = []
    for diameter in diameters:
        numerical_solution = Airway(**Constructor(
            particle_diameter=diameter,
            **extra_constructor_properties,
        ).unpack())
        deposition_values.append(
            numerical_solution.deposition_fraction()['deposition_fraction'])
    data = np.array([diameters, deposition_values])
    df = pd.DataFrame(data.T, columns=['diameter', 'deposition'])
    df.to_csv(os.path.join(RESULTS_DIR, output_filename), index=False)


def create_deposition_plot(results, literature_filename=None,
                           output_filename=None, subfigure=False):
    """
    Create a deposition plot (particle diameter vs deposition fraction)

    Parameters
    ----------
    results: str or list
        Filename or filenames from `get_deposition_fractions`
    literature_filename: str or None
        Filename of literature results to compare to
    output_filename: str
        output filename
    subfigure: bool
        Specify if the figure should be a subfigure (only for saving in .tex
        format).

    See Also
    --------
    `get_deposition_fractions`
    """
    if isinstance(results, str):
        results = [results]
    fig, ax = plt.subplots()
    if literature_filename is not None:
        literature_filename = os.path.join(
            LITERATURE_RESULTS_DIR, literature_filename)
        literature = pd.read_csv(literature_filename)
        ax.plot(
            literature['diameter'],
            literature['deposition'],
            color=COLOR,
            linewidth=LINEWIDTH,
            linestyle='--')
    results = [os.path.join(RESULTS_DIR, filename) for filename in results]

    for idx, result in enumerate(results):
        df = pd.read_csv(result)
        ax.plot(
            df['diameter']*1e6,
            df['deposition'],
            color=COLOR,
            linestyle=LINESTYLES[idx],
            linewidth=LINEWIDTH)

    ax.set_xscale('log')
    ax.xaxis.set_major_formatter(mticker.ScalarFormatter())
    ax.set_xlabel(r'Particle diameter [\si{\micro\metre}]')
    ax.set_ylabel('Deposition fraction')
    ax.set_xlim((0.01, 10))
    ax.set_ylim((0, 1))
    _handle_output_file(output_filename, subfigure=subfigure)


def create_local_deposition_plot(extra_constructor_properties, output_filename=None,
                                 literature_filename=None, subfigure=False,
                                 ):
    """
    Create a local deposition plot (generation vs deposition fraction)

    Parameters
    ----------
    extra_constructor_properties: dict
        Extra parameters for `Constructor`
    output_filename: str
        output filename
    literature_filename: str or None
        Filename of literature results to compare to
    subfigure: bool
        Specify if the figure should be a subfigure (only for saving in .tex
        format).
    """
    if isinstance(extra_constructor_properties, dict):
        extra_constructor_properties = [extra_constructor_properties]
    local_deposition_fraction = []
    fig, ax = plt.subplots()
    for idx, result in enumerate(extra_constructor_properties):
        numerical_solution = Airway(**Constructor(
            **result
        ).unpack())
        ldf = numerical_solution.deposition_fraction()[
            'local_deposition_fraction']
        local_deposition_fraction.append(ldf)
        ax.plot(range(24), ldf, color=COLOR,
                linewidth=LINEWIDTH, linestyle=LINESTYLES[idx])
    if literature_filename is not None:
        literature_filename = os.path.join(
            LITERATURE_RESULTS_DIR, literature_filename)
        literature = pd.read_csv(literature_filename)
        ax.plot(literature['generation'], literature['deposition'],
                color=COLOR, linewidth=LINEWIDTH, linestyle='--')
    ax.set_xlabel('Generation')
    ax.set_ylabel('Deposition fraction')
    ax.yaxis.set_major_locator(
        mticker.MaxNLocator(
            min_n_ticks=3, steps=[
                1, 2]))
    ax.xaxis.set_major_locator(mticker.MaxNLocator(steps=[4]))
    ax.set_ylim(bottom=0)
    ax.set_xlim((0, 23))
    _handle_output_file(output_filename, subfigure=subfigure)


def concentration_vs_time_plot(extra_constructor_properties, output_filename=None,
                               subfigure=False):
    """
    Create a concentration plot (concentration vs time)

    Parameters
    ----------
    extra_constructor_properties: dict
        Extra parameters for `Constructor`
    output_filename: str
        output filename
    subfigure: bool
        Specify if the figure should be a subfigure (only for saving in .tex
        format).
    """
    if isinstance(extra_constructor_properties, dict):
        extra_constructor_properties = [extra_constructor_properties]
    fig, ax = plt.subplots()
    for idx, result in enumerate(extra_constructor_properties):
        numerical_solution = Airway(**Constructor(
            **result
        ).unpack())
        t = numerical_solution.sol.index
        deposition_array = np.array(numerical_solution.total_deposition)
        ax.plot(t, np.cumsum(deposition_array.sum(axis=1)), color=COLOR,
                linewidth=LINEWIDTH, linestyle=LINESTYLES[idx])
    ax.set_xlabel(r'time [\si{\second}]')
    ax.set_ylabel('Normalised absorption')
    ax.set_ylim(bottom=0)
    ax.set_xlim(left=0)
    _handle_output_file(output_filename, subfigure=subfigure)


def get_dimensions(number_plots):
    """
    Return the dimensions for `matplotlib.pyplot.subplots`

    Parameters
    ----------
    number_plots: int
        Number of subplots

    Returns
    -------
    Dimensions of subplots
    """
    if number_plots < 2:
        raise ValueError("must have at least 2 plots")
    if number_plots > 9:
        raise ValueError("cannot have more than 9 plots")
    switcher = {
        2: (2,),
        3: (2, 2),
        4: (2, 2),
        5: (2, 3),
        6: (2, 3),
        7: (3, 3),
        8: (2, 4),
        9: (3, 3),
    }
    return switcher[number_plots]


def time_evolution_plot(time_steps, solutions_bundle, output_filename=None):
    """
    Subplots of concentration vs time for multiple solutions

    Parameters
    ----------
    time_steps: list
        List of time steps
    solutions_bundle: list of pandas DataFrame
        List of `Solution.sol`
    output_filename: str
        output filename

    Notes
    -----
    There is probably some duplication with `single_time_evolution_plot` and
    `compare_multi_solutions_plot`.
    """
    dimensions = get_dimensions(len(solutions_bundle))
    fig, ax = plt.subplots(*dimensions)
    times = [solutions_bundle[0][0].index[
        i*solutions_bundle[0][0].shape[0] // max(time_steps-1, 0)]
        for i in range(time_steps-1)]
    times.append(solutions_bundle[0][0].index[-1])
    for idx, solutions in enumerate(solutions_bundle):
        index = np.unravel_index(idx, dimensions)
        ax[index].set_xlabel('x')
        ax[index].set_ylabel('concentration')
        for idx2, solution in enumerate(solutions):
            solution.columns = map(float, solution.columns)
            ax[index].plot(
                solution.T[times],
                color='k',
                linestyle=LINESTYLES[idx2],
                linewidth=1)
            ax[index].set_xlim([float(solution.columns[0]),
                                float(solution.columns[-1])])
        create_labels(times, solution, ax[index])
    _handle_output_file(output_filename)


def single_time_evolution_plot(
        time_steps, solutions_bundle, output_filename=None):
    fig, ax = plt.subplots()
    times = [solutions_bundle[0].index[
        i*solutions_bundle[0].shape[0] // max(time_steps-1, 0)]
        for i in range(time_steps-1)]
    times.append(solutions_bundle[0].index[-1])
    ax.set_xlabel('x')
    ax.set_ylabel('concentration')
    for idx2, solution in enumerate(solutions_bundle):
        solution.columns = map(float, solution.columns)
        ax.plot(
            solution.T[times],
            color='k',
            linestyle=LINESTYLES[idx2],
            linewidth=1)
        ax.set_xlim([float(solution.columns[0]), float(solution.columns[-1])])
    create_labels(times, solution, ax)
    _handle_output_file(output_filename)


def compare_multi_solutions_plot(number_plots, *solutions):
    if not all(solution.shape == solutions[0].shape for solution in solutions):
        raise ValueError("all the input arrays must have same number of"
                         " dimensions")
    dimensions = get_dimensions(number_plots)
    fig, ax = plt.subplots(*dimensions)
    times = [solutions[0].index[
        i*solutions[0].shape[0] // max(number_plots-1, 0)]
        for i in range(number_plots-1)]
    times.append(solutions[0].index[-1])
    for idx, time in enumerate(times):
        index = np.unravel_index(idx, dimensions)
        for solution in solutions:
            ax[index].plot(solution.T[time])
            ax[index].set_ylim([0, 2])
            ax[index].set_xlabel('x')
            ax[index].set_ylabel('concentration')
            ax[index].set_title('{:.2f}%'.format(100*idx/(number_plots-1)))
    plt.show()


def create_inlet_velocity_plot(output_filename=None):
    """
    Plot inlet velocity vs time

    Parameters
    ----------
    output_filename: str
        output filename
    """
    constructor = Constructor()
    inlet_velocity, T = constructor.inlet_velocity, constructor.T
    fig = plt.figure()
    ax = fig.gca()
    ax.plot(T, inlet_velocity, color=COLOR, linewidth=LINEWIDTH)
    ax.set_xlabel(r'Time [\si{\second}]')
    ax.set_ylabel(r'Inlet velocity [\si{\metre\per\second}]')
    ax.set_xlim((T[0], T[-1]))
    ax.set_ylim((-1.1*max(abs(inlet_velocity)), 1.1*max(abs(inlet_velocity))))
    _handle_output_file(output_filename)


def get_velocities_df(time_indices=None):
    """
    Get DataFrame with velocity profile

    Parameters
    ----------
    time_indices: None or list
        Indices of time steps

    Returns
    -------
    pandas DataFrame
        velocity for all x and times at time_indices
    """
    constructor = Constructor()
    velocity_all, T = constructor.velocity_all, constructor.T
    x = constructor.x
    time_indices = _handle_time_indices(time_indices)
    velocities = velocity_all[time_indices, :]
    df = pd.DataFrame(velocities.T, index=x, columns=T[time_indices])
    return df


def get_reynolds_df(time_indices=None):
    """
    Get DataFrame with Reynolds numbers profile

    Parameters
    ----------
    time_indices: None or list
        Indices of time steps

    Returns
    -------
    pandas DataFrame
        Reynolds numbers for all x and times at time_indices
    """
    constructor = Constructor()
    velocity_all, T = constructor.velocity_all, constructor.T
    x = constructor.x
    time_indices = _handle_time_indices(time_indices)
    reynolds = abs(velocity_all[time_indices, :] * constructor.fluid_density
                   * constructor.get_generation_number(
                       constructor.airway_diameter_all[time_indices, :])
                   / constructor.fluid_viscosity)
    df = pd.DataFrame(reynolds.T, index=x, columns=T[time_indices])
    return df


def get_concentration_df(time_indices=None):
    """
    Get DataFrame with concentration profile

    Parameters
    ----------
    time_indices: None or list
        Indices of time steps

    Returns
    -------
    pandas DataFrame
        Concentration profile for all x and times at time_indices
    """
    numerical_solution = Airway(**Constructor().unpack())
    time_indices = _handle_time_indices(time_indices)
    df = numerical_solution.sol.T.iloc[:, time_indices]
    return df


def create_multi_plot(df, y_label=None, time_indices=None,
                      show_generations=True, output_filename=None,
                      subfigure=False, show_labels=True, x_index=-1):
    fig, ax = plt.subplots()
    ax.plot(df, color=COLOR, linewidth=0.4)
    ax.set_xlabel(r'Distance from trachea [\si{\metre}]')
    ax.set_ylabel(y_label)
    ax.set_xlim([min(df.index), max(df.index)])
    ax.xaxis.set_major_formatter(mticker.ScalarFormatter())
    if show_labels:
        create_labels(df.columns, df.T, ax, x_index)
    if show_generations:
        show_plot_generations(ax)
    _handle_output_file(output_filename, subfigure=subfigure)


def create_multiple_deposition_fractions(d):
    for filename, properties in d.items():
        print(filename)
        get_deposition_fractions(
            filename,
            number=20,
            extra_constructor_properties=properties)


def stability_plot(output_filename=None):
    def g(z, dx, dt, alpha, beta, gamma, rho):
        return (alpha - (2*beta*dt/dx**2) - (dt/dx)*gamma - dt*rho) \
            + (dt/dx**2)*beta*np.exp(1j*z) \
            + ((dt/dx)*gamma + (dt/dx**2)*beta)*np.exp(-1j*z)

    fig, ax = plt.subplots()
    # unit circle
    t = np.linspace(0, 2*np.pi, 50)
    ax.plot(np.cos(t), np.sin(t), color=COLOR, linestyle=':')

    s = np.linspace(-100, 100, 1000)
    w = g(s, 0.07, 0.01, 0.15, 0.1, 1, 0.1)
    ax.plot(w.real, w.imag, color=COLOR)
    lim = 1.2
    ax.set_xlabel(r'\(\operatorname{Re}\)')
    ax.set_ylabel(r'\(\operatorname{Im}\)')
    ax.set_xlim((-lim, lim))
    ax.set_ylim((-lim, lim))
    _handle_output_file(output_filename)


if __name__ == '__main__':
    generate_results = input('Generate results? (y/n): ')
    if generate_results.lower() != 'y':
        exit()
    create_inlet_velocity_plot()
    get_deposition_fractions('deposition_fraction.csv',
                             start=-2, stop=1, number=20)
    create_deposition_plot('deposition_fraction.csv',
                           literature_filename='Eulerian_deposition_fraction.csv',
                           output_filename="deposition_fraction.tex")
    time_indices = [10, 20, 50, 100]
    create_multi_plot(get_velocities_df(time_indices=time_indices),
                      y_label=r'Velocity [\si{\metre\per\second}]',
                      time_indices=time_indices, x_index=35,
                      output_filename='multi_velocity.tex', subfigure=True)
    create_multi_plot(get_reynolds_df(time_indices=time_indices),
                      y_label=r"Reynolds's number", time_indices=time_indices,
                      x_index=35, output_filename='multi_reynolds.tex',
                      subfigure=True)
    time_indices = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) * 10
    create_multi_plot(get_concentration_df(time_indices=time_indices),
                      y_label=r"Concentration", time_indices=time_indices,
                      show_labels=False,
                      output_filename='multi_concentration.tex')
    create_local_deposition_plot(
        {'particle_diameter': 0.01e-6, 'tidal_volume': 0.000625, 'dt': 0.01},
        literature_filename='local_deposition_0.01e-6.csv',
        output_filename='local_deposition_0.01e-6.tex', subfigure=True)
    create_local_deposition_plot(
        {'particle_diameter': 1e-6, 'tidal_volume': 0.000625, 'dt': 0.01},
        literature_filename='local_deposition_1e-6.csv',
        output_filename='local_deposition_1e-6.tex', subfigure=True)
    # Peclet plot
    solutions_bundle = [
        [pd.read_csv(
            f'../../Datasets/Results/simple_peclet/{T}-{P}-peclet.csv',
            index_col=0)
         for T in ['analytical', 'numerical']] for P in [0.003, 0.03, 0.3, 3]
    ]
    for name, bundle in zip([0.003, 0.03, 0.3, 3], solutions_bundle):
        single_time_evolution_plot(
            5, bundle, f'peclet-{name}.tex')

    # Results generation
    # Effect of geometry rescaling:
    d = {
        'deposition_fraction-default.csv': {},
        'deposition_fraction-rescale_geometry-False.csv': {
            'rescale_geometry': False,
        },
        'deposition_fraction-rescale_geometry-False-FRC-2400.csv': {
            'rescale_geometry': False,
            'functional_residual_capacity': 0.0024,
        },
    }
    create_multiple_deposition_fractions(d)
    create_deposition_plot(
        [
            'deposition_fraction-default.csv',
            'deposition_fraction-rescale_geometry-False.csv',
            'deposition_fraction-rescale_geometry-False-FRC-2400.csv',
        ],
        output_filename='rescale_geometry.tex',
        subfigure=True)
    create_local_deposition_plot(
        [{'particle_diameter': 4e-6, 'rescale_geometry': False},
         {'particle_diameter': 4e-6}],
        output_filename='rescale_geometry_local.tex',
        subfigure=True)

    # Effect of different geometries
    from constants import yeh_schum_length, yeh_schum_radius
    d = {
        'deposition_fraction-YS.csv': {
            'generation_length': yeh_schum_length,
            'generation_radius': yeh_schum_radius,
            'rescale_geometry': False,
        },
    }
    create_multiple_deposition_fractions(d)
    create_deposition_plot(
        [
            'deposition_fraction-rescale_geometry-False.csv',
            'deposition_fraction-YS.csv',
        ],
        output_filename='weibel_vs_yehschum.tex')

    # Effect of time-dependent geometry vs fixed
    d = {
        'deposition_fraction-fixed_geometry.csv': {
            'constant_diameter': True,
        },
    }
    create_multiple_deposition_fractions(d)
    create_deposition_plot(
        [
            'deposition_fraction-default.csv',
            'deposition_fraction-fixed_geometry.csv',
        ],
        output_filename='time_dependent_geometry.tex',
        subfigure=True)
    create_local_deposition_plot(
        [{'particle_diameter': 0.3e-6},
         {'particle_diameter': 0.3e-6, 'constant_diameter': True}],
        output_filename='fixed_geometry_local.tex',
        subfigure=True)

    # Effect of tidal volume
    d = {}
    values = [0.0005, 0.001, 0.002, 0.003]
    for value in values:
        d[f'deposition_fraction-tidal_volume-{value}.csv'] = {
            'tidal_volume': value,
            'functional_residual_capacity': 0.003,
        }
    create_multiple_deposition_fractions(d)
    create_deposition_plot(
        [
            'deposition_fraction-tidal_volume-0.0005.csv',
            'deposition_fraction-tidal_volume-0.001.csv',
            'deposition_fraction-tidal_volume-0.002.csv',
            'deposition_fraction-tidal_volume-0.003.csv',
        ],
        output_filename='tidal_volume.tex',
        subfigure=True)

    concentration_vs_time_plot(
        [{'t_final': 60.0, 'particle_diameter': 0.3e-6},
         {'t_final': 60.0, 'tidal_volume': 0.003, 'particle_diameter': 0.3e-6}],
        output_filename='concentration_vs_time.tex',
        subfigure=True)

    # Effect of particle density
    d = {}
    values = np.linspace(500, 2000, 4)
    for value in values:
        d[f'deposition_fraction-particle_density-{value}.csv'] = {
            'particle_density': value,
            'functional_residual_capacity': 0.003,
        }
    create_multiple_deposition_fractions(d)
    create_deposition_plot(
        [
            'deposition_fraction-particle_density-500.csv',
            'deposition_fraction-particle_density-1000.csv',
            'deposition_fraction-particle_density-1500.csv',
            'deposition_fraction-particle_density-2000.csv',
        ],
        output_filename='particle_density.tex')

    stability_plot("stability.tex")

    # Effect of breathing rate
    breathing_rate = [10/60, 12/60, 15/60]
    # NOTE: you would *expect* the following code to work, but it does not
    # (see: https://stackoverflow.com/a/34021333/7770654). Therefore, we use
    # the partial function from functools.
    #
    # breathing_pattern = [
    #     lambda t: -np.cos(t*br*2*np.pi) for br in breathing_rate
    # ]
    from functools import partial
    breathing_pattern = [
        partial(lambda t, coef: -np.cos(t*coef*2*np.pi), coef=br)
        for br in breathing_rate
    ]
    d = {}
    for br, bp in zip(breathing_rate, breathing_pattern):
        d[f'deposition_fraction-breathing_rate-{br}.csv'] = {
            'functional_residual_capacity': 0.003,
            'breathing_rate': br,
            'breathing_pattern': bp,
            't_final': 1/br + 0.1,
        }
    create_multiple_deposition_fractions(d)
    create_deposition_plot(
        [
            'deposition_fraction-breathing_rate-0.16666666666666666.csv',
            'deposition_fraction-breathing_rate-0.2.csv',
            'deposition_fraction-breathing_rate-0.25.csv',
        ],
        output_filename='breathing_rate.tex')
