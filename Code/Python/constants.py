"""
Physical constants and accompanying Weibel geometry.
"""


import numpy as np
import scipy.constants
from scipy import integrate


# Weibel model
gravity_angle = np.ones(24) * 45 * np.pi/180
branching_angle = np.ones(24) * 30 * np.pi/180
generations = np.arange(0, 24)
number_airways_per_generation = np.power(2, generations)
weibel_length = np.array([
    12.0e-2, 4.76e-2, 1.90e-2, 0.76e-2, 1.27e-2, 1.07e-2,
    0.90e-2, 0.76e-2, 0.64e-2, 0.54e-2, 0.46e-2, 0.39e-2,
    0.33e-2, 0.27e-2, 0.16e-2, 0.133e-2, 0.112e-2, 0.093e-2,
    0.083e-2, 0.070e-2, 0.070e-2, 0.070e-2, 0.067e-2, 0.075e-2,
])
weibel_radius = np.array([
    1.80e-2, 1.22e-2, 0.83e-2, 0.56e-2, 0.45e-2, 0.35e-2,
    0.28e-2, 0.23e-2, 0.186e-2, 0.154e-2, 0.130e-2, 0.109e-2,
    0.095e-2, 0.082e-2, 0.074e-2, 0.050e-2, 0.049e-2, 0.040e-2,
    0.038e-2, 0.036e-2, 0.034e-2, 0.031e-2, 0.029e-2, 0.025e-2
]) / 2
yeh_schum_length = np.array([
    10e-2, 4.36e-2, 1.78e-2, 0.965e-2, 0.995e-2, 1.01e-2, 0.89e-2, 0.962e-2,
    0.867e-2, 0.667e-2, 0.556e-2, 0.446e-2, 0.359e-2, 0.275e-2, 0.212e-2,
    0.168e-2, 0.134e-2, 0.12e-2, 0.092e-2, 0.08e-2, 0.07e-2, 0.063e-2,
    0.057e-2, 0.053e-2
])
yeh_schum_radius = np.array([
    2.01e-2, 1.56e-2, 1.13e-2, 0.827e-2, 0.651e-2, 0.574e-2, 0.435e-2, 0.373e-2,
    0.322e-2, 0.257e-2, 0.198e-2, 0.156e-2, 0.118e-2, 0.092e-2, 0.073e-2,
    0.06e-2, 0.054e-2, 0.05e-2, 0.047e-2, 0.045e-2, 0.044e-2, 0.044e-2,
    0.043e-2, 0.043e-2
])
yeh_schum_gravity_angle = np.array([
    0, 20, 31, 43, 39, 39, 40, 36, 39, 45, 43, 45,
    45, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60
]) * np.pi/180
yeh_schum_branching_angle = np.array([
    0, 33, 34, 22, 20, 18, 19, 22, 28, 22, 33, 34,
    37, 39, 39, 51, 45, 45, 45, 45, 45, 45, 45, 45
]) * np.pi/180
cumulative_length = np.append(0, np.cumsum(weibel_length))


# Grid
N = 20
N_zeros = np.zeros(N*len(generations))
# Time
theta = 1
dt = 0.1
t_start = 0
t_final = 5.1
# General
EPS = 1e-15
run_flags = ["diffusion", "advection", "deposition"]

# Boundary conditions
N_r = 0
right_boundary = ("neumann", N_r)
initial_condition = N_zeros

# Physics constants
boltzmann_constant = scipy.constants.Boltzmann
gravity = scipy.constants.g

# Particle constants
particle_density = 1000  # from Wikipedia
particle_diameter = 10e-6
# Fluid properties
fluid_density = 1
fluid_viscosity = 18.1e-6  # viscosity of air
fluid_gas_temperature = 293
mean_free_path = 0.066e-6

# Lung constants
functional_residual_capacity = 0.0033
tidal_volume = 0.001
breathing_rate = 12/60  # 12 breaths per minute
# We start the respiratory cycle in the inspiration phase.
breathing_pattern = lambda t: -np.cos(t*breathing_rate*2*np.pi)


class Constructor():
    """
    Represents the parameters required for airway calculations.

    All parameters are always in SI units. The eventual output is in the from
    the `self.unpack` function.

    Parameters
    ----------
    initial_condition: numpy array
        Initial concentration in the respiratory tract
    right_boundary: tuple
        Tuple of boundary type and value. Should always be Neumann and 0, stems
        from when the programme could solve arbitrary airways.
    N: int
        Number of grid points per generation
    theta: float in [0, 1]
        Parameter theta for the theta-method. A value of 0 is completely
        explicit, and a value of 1 is completely implicit.
    dt: float
        Time step size
    t_start: float
        Initial time step (should be zero)
    t_final: float
        Final time step
    fluid_density: float
        Density of air
    fluid_viscosity: float
        Viscosity of air
    fluid_gas_temperature: float
        Temperature of air
    mean_free_path: float
        Mean free path (lambda) in air
    particle_density: float
        Density of the particles (not concentration)
    particle_diameter: float
        Diameter of the particles
    generations: numpy array
        Generation numbers (0-23)
    generation_radius: numpy array
        Generation radii
    generation_length: numpy array
        Generation lengths
    number_airways_per_generation: numpy array
        Number of airways per generation
    branching_angle: numpy array
        Branching angles per generation
    gravity_angle: numpy array
        Gravity angles per generation
    functional_residual_capacity: float
        V_FRC of the patient
    tidal_volume: float
        V_T of the patient
    breathing_rate: float
        Breathing rate of the patient
    breathing_pattern: function
        periodic function, f: [0, infty] -> [-1, 1]
    run_flags: list
        List of run flags (diffusion, advection, or deposition)
    constant_diameter: bool
        Whether to include expanding lung effects
    rescale_geometry: bool
        Whether to rescale the model according to Weibel's original size.
    """
    ALVEOLI_INDEX = 16
    LEFT_CONCENTRATION = 1
    LEFT_FLUX = 0
    WEIBEL_ORIGINAL_FRC = 0.0048

    def __init__(
        self,
        initial_condition=initial_condition,
        right_boundary=right_boundary,
        N=N,  # per generation
        theta=theta,
        dt=dt,
        t_start=t_start,
        t_final=t_final,
        fluid_density=fluid_density,
        fluid_viscosity=fluid_viscosity,
        fluid_gas_temperature=fluid_gas_temperature,
        mean_free_path=mean_free_path,
        particle_density=particle_density,
        particle_diameter=particle_diameter,
        generations=generations,
        generation_radius=weibel_radius,
        generation_length=weibel_length,
        number_airways_per_generation=number_airways_per_generation,
        branching_angle=branching_angle,
        gravity_angle=gravity_angle,
        functional_residual_capacity=functional_residual_capacity,
        tidal_volume=tidal_volume,
        breathing_rate=breathing_rate,
        breathing_pattern=breathing_pattern,
        run_flags=run_flags,
        constant_diameter=False,
        rescale_geometry=True,
    ):
        args = locals().copy()
        del args['self']
        for key, value in args.items():
            setattr(self, key, value)
        # Variables constant for the duration of the simulation
        self.T = np.concatenate([np.arange(self.t_start, self.t_final, self.dt),
                                 [self.t_final]])
        if self.rescale_geometry:
            self.scale_geometry()
        self.cumulative_length = np.append(
            0, np.cumsum(self.generation_length))
        self.x = np.concatenate([
            np.linspace(self.cumulative_length[i], self.cumulative_length[i+1],
                        self.N, endpoint=False)
            for i in range(len(self.cumulative_length)-1)])
        # The grid spacing below is edge based: i.e. the spacing is defined by
        # which edge is in the "center".
        self.grid_spacing = self.x[1:]-self.x[:-1]
        self.lung_volume = self.get_lung_volume()
        self.A_A = self.get_generation_number(
            self.number_airways_per_generation
            * np.pi
            * np.power(self.generation_radius, 2)
        )
        # Variables that can change for each time step during the simulation
        self.airway_diameter_all = self.get_airway_diameter()
        if self.constant_diameter:
            self.airway_diameter_all[:, :] = self.generation_radius*2
        self.A_T_all = self.get_generation_number(
            self.number_airways_per_generation
            * np.pi
            * np.power(self.airway_diameter_all, 2)
            / 4
        )
        self.inlet_velocity = self.get_inlet_velocity()
        self.velocity_all = self.get_velocity_profile()
        self.left_boundary_all = self.get_left_boundary()
        self.initial_condition = self.initial_condition
        self.cunningham_slip_correction = self.get_cunningham_slip_correction()
        self.particle_relaxation_time = self.get_particle_relaxation_time()
        self.gravity_angle = self.get_generation_number(self.gravity_angle)
        self.terminal_settling_velocity = self.get_terminal_settling_velocity()
        self.gravitational_settling_velocity = (
            self.get_gravitational_settling_velocity())
        self.brownian_diffusion_coefficient = self.get_brownian_diffusion_coefficient()

    def scale_geometry(self):
        """
        Scale geometry according to Weibels original lung volume of 4.8 litre
        """
        self.generation_radius = self.generation_radius * np.power(
            self.functional_residual_capacity / self.WEIBEL_ORIGINAL_FRC, 1/3)
        self.generation_length = self.generation_length * np.power(
            self.functional_residual_capacity / self.WEIBEL_ORIGINAL_FRC, 1/3)

    def get_generation_number(self, quantity):
        """
        Quantity extended to the grid points instead of generation numbers

        Take a quantity of length 24 (amount of generations), and conform it to
        `self.x`, while taking into account which generation each value is in.

        If the quantity is 2-dimensional, the function is applied recursively
        (row-wise), and the output will be 2-dimensional.

        Parameters
        ----------
        Quantity: numpy array
            Quantity on generation numbers

        Returns
        -------
        numpy array
            Quantity on grid points
        """
        if len(quantity.shape) > 1:
            return np.array([self.get_generation_number(quantity[i, :])
                             for i in range(quantity.shape[0])])
        bin = np.digitize(self.x, self.cumulative_length)-1
        return quantity[bin]

    def get_lung_volume(self):
        """
        Calculate the lung volume at time steps `self.T`

        Returns
        -------
        numpy array
            Lung volume
        """
        lung_volume = (self.functional_residual_capacity
                       + self.tidal_volume/2 * (1+self.breathing_pattern(self.T)))
        return lung_volume

    def get_airway_diameter(self):
        """
        Calculate the airway diameter due to breathing mechanics.

        Returns
        -------
        numpy array
            Airway diameter at every x and t.
        """
        airway_diameter = self.generation_radius * 2
        d_T = np.zeros([len(self.lung_volume), len(airway_diameter)])
        d_T[:, :] = np.copy(airway_diameter)
        # Airway diameter only varies with time from generation >= ALVEOLI_INDEX
        # (alveolated airways)
        d_T[:, self.ALVEOLI_INDEX:] = (
            d_T[:, self.ALVEOLI_INDEX:]
            * np.power(self.lung_volume/self.functional_residual_capacity, 1/3)
            .reshape(-1, 1))
        return d_T

    def get_alveoli_index(self):
        """
        Find the alveoli index on the spatial grid.

        Returns
        -------
        int
            index where the alveoli start
        """
        return np.nonzero(
            (self.x > self.cumulative_length[self.ALVEOLI_INDEX]) == 1)[0][0]

    def get_inlet_velocity(self):
        """
        Calculate the inlet velocity at all time steps

        Returns
        -------
        numpy array
            Inlet velocity at all time steps
        """
        volume_derivative = np.gradient(self.lung_volume, self.dt)
        inlet_velocity = volume_derivative / self.A_A[0]
        return inlet_velocity

    def get_velocity_profile(self):
        """
        Calculate the velocity profile u(x, t)

        Returns
        -------
        numpy array
            Velocity profile at all x and t
        """
        alveoli_index = self.get_alveoli_index()
        copy_array = self.inlet_velocity.reshape(-1, 1) * self.A_A[0]
        u = np.hstack(
            [np.copy(copy_array) for i in range(self.A_T_all.shape[1])])
        # For the time dependent part:
        deriv = np.gradient(self.A_T_all[:, alveoli_index:], self.dt, axis=0)
        u /= self.A_A
        for idx_t in range(len(self.T)):
            for idx_x in range(alveoli_index, len(self.x)):
                u[idx_t, idx_x] = (u[idx_t, alveoli_index-1] * self.A_A[alveoli_index-1]
                                   - integrate.trapz(
                    deriv[idx_t, :idx_x-alveoli_index+1],
                    self.x[alveoli_index:idx_x+1]
                )) / self.A_A[idx_x]
        return u

    def get_left_boundary(self):
        """
        Determine the left boundary condition at all time steps

        The boundary condition changes from inspiration to expiration and vice
        versa

        Returns
        -------
        list of tuples
            Left boundary condition at all time steps.
        """
        left_boundary_all = list(self.velocity_all[:, 0] >= 0)
        left_boundary_all = [("dirichlet", self.LEFT_CONCENTRATION)
                             if bc else ("neumann", self.LEFT_FLUX)
                             for bc in left_boundary_all]
        return left_boundary_all

    def get_particle_relaxation_time(self):
        """
        Calculate particle relaxation time.

        Returns
        -------
        float
            particle relaxation time
        """
        # https://aerosol.ees.ufl.edu/aerosol_trans/section07.html
        return (self.particle_density * np.power(self.particle_diameter, 2)
                * self.cunningham_slip_correction
                / (18 * self.fluid_viscosity))

    def get_cunningham_slip_correction(self):
        """
        Calculate Cunningham slip correction factor.


        Returns
        -------
        float
            Cunningham slip correction factor
        """
        # https://aerosol.ees.ufl.edu/aerosol_trans/section06_c.html
        cunningham_slip_correction = (
            1 + (self.mean_free_path/self.particle_diameter)
            * (2.34 + 1.05 * np.exp(
                -0.39 * self.particle_diameter/self.mean_free_path)))
        return cunningham_slip_correction

    def get_gravitational_settling_velocity(self):
        """
        Calculate the gravitational settling velocity.

        Returns
        -------
        numpy array
            Gravitational settling velocity for all x
        """
        return self.terminal_settling_velocity * np.sin(self.gravity_angle)

    def get_terminal_settling_velocity(self):
        """
        Calculate the terminal settling velocity.

        Returns
        -------
        float
            Terminal settling velocity
        """
        return (self.particle_density * self.particle_diameter**2 * gravity
                * self.cunningham_slip_correction / (18 * self.fluid_viscosity))

    def get_brownian_diffusion_coefficient(self):
        """
        Calculate the Brownian diffusion coefficient from Stokes-Einsten equation.

        Returns
        -------
        float
            Brownian diffusion coefficient
        """
        return ((boltzmann_constant * self.fluid_gas_temperature
                 * self.cunningham_slip_correction)
                / (3 * np.pi * self.fluid_viscosity * self.particle_diameter))

    def unpack(self):
        """
        Unpack all constants for use in Airway or ExactAirway

        Returns
        -------
        dict
            Dict of parameters
        """
        boundaries = {
            'initial_condition': self.initial_condition,
            'left_boundary_all': self.left_boundary_all,
            'right_boundary': self.right_boundary,
        }
        space_discretisation = {
            'x': self.x,
            'grid_spacing': self.grid_spacing,
        }
        time_integration = {
            'theta': self.theta,
            'T': self.T,
            'dt': self.dt,
        }
        general = {
            'A_T_all': self.A_T_all,
            'A_A': self.A_A,
            'velocity_all': self.velocity_all,
            'fluid_density': self.fluid_density,
            'airway_diameter_all': self.get_generation_number(
                self.airway_diameter_all),
        }
        meta = {
            'run_flags': self.run_flags,
        }
        deposition = {
            'number_airways_per_generation': self.get_generation_number(
                self.number_airways_per_generation),
            'branching_angle': self.get_generation_number(self.branching_angle),
            'generation_length': self.get_generation_number(self.generation_length),
            'cumulative_length': self.get_generation_number(self.cumulative_length),
            'fluid_viscosity': self.fluid_viscosity,
            'gravitational_settling_velocity': self.gravitational_settling_velocity,
            'particle_relaxation_time': self.particle_relaxation_time,
            'brownian_diffusion_coefficient': self.brownian_diffusion_coefficient,
            'breathing_rate': self.breathing_rate,
        }
        return {
            'boundaries': boundaries,
            'space_discretisation': space_discretisation,
            'time_integration': time_integration,
            'general': general,
            'meta': meta,
            'deposition': deposition,
        }
